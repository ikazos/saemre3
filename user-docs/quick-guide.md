# Quick guide

This is a quick guide to some `saemre3` commands.

## Starting `saemre3`

For now, `saemre3` supports a REPL-like user interface. Run `saemre3` in your
terminal and a prompt will appear:

```
saemre3 -- type in a command. To get help, run `help`.
 >> _
```

You can type in commands to tell `saemre3` what to do. Each command starts with
a (valid) command name and a whitespace-separated list of arguments.

## Hear a 440Hz sine wave

Let's create a module, specifically a sine wave generator (called `sine` in
`saemre3`):

```
add sine sine()
```

We just used the `add` command. The first `sine` is the name we wish to give to
the new module, and the second `sine()` is called a module initializer list --
it specifies the type of the module we wish to create, which is also called
`sine`.

The module `sine` has 4 inputs (`freq`, `amp`, `phase`, `offset`). Within the
parentheses `()` in the module initializer list `sine()`, you can optionally
provide your own default values for these inputs, but the default default
values are fine for now. The default defaults are `freq`=440, `amp`=1,
`phase`=0, `offset`=0.

Let's create a digital-to-analog converter (DAC), which is also a kind of
module, named `dac`:

```
add dac dac()
```

`dac` has 1 input named `in`. It receives a digital audio signal through `in`
and relays that to your audio device, so that you can hear the signal. The sine
wave generator `sine` we have up and running right now is producing a sine wave
at 440Hz and sending it through its output `out`. We can connect the output
`out` from `sine` to the input `in` of `dac`:

```
connect -sine.out -dac.in
```

`-sine.out` and `-dac.in` are selectors -- a kind of syntax used to specify
inputs/outputs on a named module. For example, `-sine.out` refers to output
`out` of module `sine`, and `-dac.in` refers to input `in` of module `dac`.

Now you should hear the sine wave!

## Modulation example: amplitude modulation

Let's add another wave and modulate the amplitude of the sine wave with it. We
will create a `sawup`, an upward sawtooth wave generator, and call it `ampmod`:

```
add ampmod sawup(freq=2, amp=0.5, offset=0.5)
```

`(freq=2, amp=0.5, offset=0.5)` specifies the values we wish to provide as
default values for the inputs of `sawup`. If we didn't specify anything and
just ran `add ampmod sawup`, we would get a 440Hz wave with amplitude 1, phase
0 and offset 0. The amplitude of the resulting waveform would vary between -1
and 1. Since we will be using `ampmod` to modulate the amplitude of the sine
wave, we want to its own amplitude to stay in the range [0, 1]. This is why we
set `amp`=0.5 and `offset`=0.5:

*   Original `sawup` range: [-1, 1]
*   Multiply this by `amp`=0.5: [-0.5, 0.5]
*   Offset this by `offset`=0.5: [0, 1]

We connect the output `out` of `ampmod` to the amplitude input `amp` of `sine`:

```
connect -ampmod.out -sine.amp
```

Now you should hear the original sine wave with amplitude modulation.