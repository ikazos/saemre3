# saemre3

`saemre3` is an audio programming environment.

## Install

To get started, you need (1) `cargo`, (2) a nightly Rust compiler and (3) `git`.

### If you have all of those 3 things and you know what you are doing

```
rustup default nightly
git clone https://gitlab.com/ikazos/saemre3.git
cd saemre3
cargo run
```

### If you don't

Follow these steps:

1.  Get the Rust toolchain at [https://www.rust-lang.org/tools/install]. The Rust toolchain contains `cargo` (the Rust package manager), `rustc` (the Rust compiler) and `rustup` (the Rust toolchain manager) and possibly other tools. Follow the instructions there.

2.  By default, running `rustc` invokes the _stable_ compiler, but for `saemre3` you will need the _nightly_ compiler. Run the following command, which installs the nightly compiler and sets it to the default compiler (i.e. the compiler invoked when you run `rustc`):

    ```
    $ rustup default nightly
    ```

    You can always switch it back to the stable compiler by running the same command with `stable` instead of `nightly`.
    
    If you wish to get the nightly compiler __without__ setting it to the default, run this instead:

    ```
    $ rustup toolchain install nightly
    ```

3.  If you don't have `git`, get it at [https://git-scm.com/downloads]. Unless you are using Windows, it is unlikely that `git` is not installed in your operating system.

4.  Run the following to clone the `saemre3` repository:

    ```
    $ git clone https://gitlab.com/ikazos/saemre3.git
    ```

    This clones the repository into a directory called `saemre3` in your current directory.

5.  Open `saemre3`:

    ```
    $ cd saemre3
    ```

6.  Build and run!

    ```
    $ cargo run
    ```

    If your nightly compiler isn't the default compiler (see step 2), run:

    ```
    $ rustup run nightly cargo run
    ```

## Quick guide

Try these command sequences out. For a more detailed walk-through of what each
command does and how `saemre3` works, see [the quick guide in the user docs](user-docs/quick-guide.md).

```
add sine sine()
add dac dac()
connect -sine.out -dac.in
add ampmod sawup(freq=2, amp=.5, offset=.5)
connect -ampmod.out -sine.amp
exit
```

## Troubleshooting

This software is experimental and is under heavy development. It is also
supposed to run smoothly across different platforms, but this is not the case
(yet). As such, bugs and errors are common. Issues are welcome!

If you cannot run `saemre3`, try running one of the examples. See more about
examples in [the examples README](examples/README.md).