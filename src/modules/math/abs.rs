use crate::data::{ DataPoint, DataSlice, DataType };
use crate::modules::{ Module };

pub struct Abs {}

impl Abs {
    pub fn new() -> Self {
        Self {}
    }
}

const INPUT_TYPE_DEFAULTS: [(&'static str, DataType, Option<DataPoint>); 1] = [
    ("x", DataType::F64, None),
];

const OUTPUT_TYPES: [(&'static str, DataType); 1] = [
    ("out", DataType::F64),
];

impl Module for Abs {
    fn reset(&mut self) {}

    fn compute(&mut self, args: &DataSlice, dst: &mut DataSlice) {
        let x = match args[0] {
            DataPoint::F64(x) => x,
            _ => panic!(),
        };

        let val = x.abs();

        dst[0] = DataPoint::F64(val);
    }

    fn input_type_defaults(&self) -> Box<dyn Iterator<Item=(&str, DataType, Option<DataPoint>)> + Send + Sync + '_> {
        Box::new(
            INPUT_TYPE_DEFAULTS.iter()
                .map(|(n, it, d)| (*n, *it, *d))
        )
    }

    fn output_types(&self) -> Box<dyn Iterator<Item=(&str, DataType)> + Send + Sync + '_> {
        Box::new(
            OUTPUT_TYPES.iter()
                .map(|(n, ot)| (*n, *ot))
        )
    }
}