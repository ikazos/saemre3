use crate::data::{ DataPoint, DataSlice, DataType };
use crate::modules::{ Module };

pub struct Min {}

impl Min {
    pub fn new() -> Self {
        Self {}
    }
}

const INPUT_TYPE_DEFAULTS: [(&'static str, DataType, Option<DataPoint>); 2] = [
    ("a", DataType::F64, None),
    ("b", DataType::F64, None),
];

const OUTPUT_TYPES: [(&'static str, DataType); 1] = [
    ("out", DataType::F64),
];

impl Module for Min {
    fn reset(&mut self) {}

    fn compute(&mut self, args: &DataSlice, dst: &mut DataSlice) {
        let a = match args[0] {
            DataPoint::F64(a) => a,
            _ => panic!(),
        };

        let b = match args[1] {
            DataPoint::F64(b) => b,
            _ => panic!(),
        };

        let val = a.min(b);

        dst[0] = DataPoint::F64(val);
    }

    fn input_type_defaults(&self) -> Box<dyn Iterator<Item=(&str, DataType, Option<DataPoint>)> + Send + Sync + '_> {
        Box::new(
            INPUT_TYPE_DEFAULTS.iter()
                .map(|(n, it, d)| (*n, *it, *d))
        )
    }

    fn output_types(&self) -> Box<dyn Iterator<Item=(&str, DataType)> + Send + Sync + '_> {
        Box::new(
            OUTPUT_TYPES.iter()
                .map(|(n, ot)| (*n, *ot))
        )
    }
}