mod dac;
mod generic;
//  mod record;
//  mod writer;

//  mod math;
mod osc;

pub use dac::Dac;
pub use generic::Generic;
//  pub use record::Record;
//  pub use writer::Writer;

//  pub use math::{ Abs, Add, Div, Exp, Fmod, Ln, Max, Min, Mul, Sub };
pub use osc::{ SawUp, Sine, Square, Tri };

use crate::data::{ DataPoint, DataSlice, DataType };
use crate::io::{ Input };
use crate::io::{ Output };

use std::fmt;

use async_trait::async_trait;
use tokio::task::{ JoinHandle };

#[derive(Debug, Clone)]
pub enum SetInputError {
    InputDoesNotExist {
        name: String,
    },

    ParseError {
        value: String,
        data_type: DataType,
    },
}

impl fmt::Display for SetInputError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &SetInputError::ParseError { ref value, ref data_type, } =>
                write!(f, "could not parse value \"{}\" into a value of type {}", value, data_type),

            &SetInputError::InputDoesNotExist { ref name, } =>
                write!(f, "input \"{}\" does not exist", name),
        }
    }
}

#[async_trait]
//  Why add supertraits `Send` and `Sync`?
//  https://docs.rs/async-trait/0.1.42/async_trait/index.html#dyn-traits
//  https://github.com/rust-lang/rust/issues/51443#issuecomment-653959979
pub trait Runner: Send + Sync {
    //  The lifetime `'_` specifies that the trait object captures data from
    //  argument `self`.
    fn inputs(&self) -> Box<dyn Iterator<Item=(&str, &Input)> + Send + Sync + '_>;
    fn outputs(&self) -> Box<dyn Iterator<Item=(&str, &Output)> + Send + Sync + '_>;

    fn inputs_mut(&mut self) -> Box<dyn Iterator<Item=(&str, &mut Input)> + Send + Sync + '_>;
    fn outputs_mut(&mut self) -> Box<dyn Iterator<Item=(&str, &mut Output)> + Send + Sync + '_>;

    fn input_types(&self) -> Box<dyn Iterator<Item=(&str, DataType)> + Send + Sync + '_>;
    fn output_types(&self) -> Box<dyn Iterator<Item=(&str, DataType)> + Send + Sync + '_>;

    async fn set_input_to_default(&mut self, name: &str, value: DataPoint) -> Result<(), SetInputError> {
        let input = self.inputs_mut()
            .find(|(n, _)| *n == name)
            .unwrap()
            .1;

        input.set_default(Some(value));
        input.unset_receiver().await;

        Ok(())
    }

    async fn start(&mut self) -> Result<JoinHandle<()>, String>;
    async fn stop(&mut self) -> Result<(), String>;

    async fn unset_inputs(&self) {
        for input in self.inputs() {
            input.1.unset_receiver().await;
        }
    }

    async fn unset_outputs(&self) {
        for output in self.outputs() {
            output.1.unset_sender().await;
        }
    }
}

pub trait Module {
    fn input_type_defaults(&self) -> Box<dyn Iterator<Item=(&str, DataType, Option<DataPoint>)> + Send + Sync + '_>;
    fn output_types(&self) -> Box<dyn Iterator<Item=(&str, DataType)> + Send + Sync + '_>;

    fn reset(&mut self);
    fn compute(&mut self, args: &DataSlice, dst: &mut DataSlice);
}