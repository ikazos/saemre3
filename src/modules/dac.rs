use crate::data::{ DataType };
use crate::io::{ Input, Output };
use crate::modules::{ Runner };

use std::collections::{ HashMap };
use std::iter;
use std::sync::{ Arc, Condvar, Mutex };

use async_trait::async_trait;

use cpal::{ BufferSize, SampleRate, StreamConfig };
use cpal::traits::{ DeviceTrait, HostTrait, StreamTrait };

use tokio::task::{ spawn_blocking, JoinHandle };

pub struct Dac {
    pub inputs: HashMap<String, Input>,
    stop: Arc<(Mutex<bool>, Condvar)>,

    buffer_size: Option<u32>,
}

const INPUT_TYPES: [(&'static str, DataType); 1] = [
    ("in", DataType::F64),
];

impl Dac {
    pub fn new(buffer_size: Option<u32>) -> Self {
        let inputs =
            vec![ (format!("in"), Input::new(None)) ]
                .into_iter().collect();

        Self {
            inputs,
            stop: Arc::new((Mutex::new(false), Condvar::new())),

            buffer_size,
        }
    }
}

fn pick_sample_rate(min_sample_rate: SampleRate, max_sample_rate: SampleRate) -> SampleRate {
    let min_sample_rate: u32 = min_sample_rate.0;
    let max_sample_rate: u32 = max_sample_rate.0;

    if (44100 >= min_sample_rate) && (44100 <= max_sample_rate) {
        SampleRate(44100)
    }
    else if (48000 >= min_sample_rate) && (48000 <= max_sample_rate) {
        SampleRate(48000)
    }
    else {
        SampleRate(max_sample_rate)
    }
}

#[async_trait]
impl Runner for Dac {
    fn inputs(&self) -> Box<dyn Iterator<Item=(&str, &Input)> + Send + Sync + '_> {
        Box::new(
            self.inputs.iter()
                .map(|(n, i)| (n.as_str(), i))
        )
    }

    fn outputs(&self) -> Box<dyn Iterator<Item=(&str, &Output)> + Send + Sync + '_> {
        Box::new(iter::empty())
    }

    fn inputs_mut(&mut self) -> Box<dyn Iterator<Item=(&str, &mut Input)> + Send + Sync + '_> {
        Box::new(
            self.inputs.iter_mut()
                .map(|(n, i)| (n.as_str(), i))
        )
    }

    fn outputs_mut(&mut self) -> Box<dyn Iterator<Item=(&str, &mut Output)> + Send + Sync + '_> {
        Box::new(iter::empty())
    }

    fn input_types(&self) -> Box<dyn Iterator<Item=(&str, DataType)> + Send + Sync + '_> {
        Box::new(
            INPUT_TYPES.iter()
                .map(|(n, it)| (*n, *it))
        )
    }

    fn output_types(&self) -> Box<dyn Iterator<Item=(&str, DataType)> + Send + Sync + '_> {
        Box::new(iter::empty())
    }

    async fn start(&mut self) -> Result<JoinHandle<()>, String> {
        let input = self.inputs["in"].clone();
        let stop = self.stop.clone();
        let buffer_size = self.buffer_size;

        let handle = spawn_blocking(move || {
            let host = cpal::default_host();
            //  println!("Host id name: {}", host.id().name());

            let device = host.default_output_device()
                .expect("no output device available");
            //  println!("Device name: {}", device.name().unwrap());

            let mut supported_configs_range = device.supported_output_configs()
                .expect("error while querying configs");

            let supported_config_range = supported_configs_range.next()
                .expect("no supported config?!");
        
            let min_sample_rate = supported_config_range.min_sample_rate();
            let max_sample_rate = supported_config_range.max_sample_rate();
            let sample_rate = pick_sample_rate(min_sample_rate, max_sample_rate);
        
            let supported_config = supported_config_range
                .with_sample_rate(sample_rate);

            let mut config: StreamConfig = supported_config.into();
            if let Some(dbs) = buffer_size {
                config.buffer_size = BufferSize::Fixed(dbs);
            }

            let _sample_rate = (config.sample_rate.0) as usize;
            let channel_count = config.channels as usize;

            //  println!("Sample rate: {}", sample_rate);
            //  println!("Channel count: {}", channel_count);

            let stream = device.build_output_stream(
                &config,
                move |data: &mut [f32], _cbinfo: &cpal::OutputCallbackInfo| {
                    // react to stream events and read or write stream data here.
                    let channel_len: usize = data.len() / channel_count;

                    let mut nvals_read: usize = 0;
                    let mut chunks = data.chunks_exact_mut(channel_count);
                    while nvals_read < channel_len {
                        match input.blocking_read_value() {
                            Some(value) => {
                                chunks.next().unwrap().fill(value.to_f32());
                                nvals_read += 1;
                            },

                            //  If we can't read any more values, stop trying
                            //  and skip this iteration.
                            None => {
                                for slice in chunks {
                                    slice.fill(0.0);
                                }
                                break;
                            },
                        }
                    }
                },
                move |err| {
                    // react to errors here.
                    eprintln!("Error! {}", err);
                },
            )
                .unwrap();
    
            stream.play().unwrap();

            let (stop, cvar) = &*stop;
            let mut stop = stop.lock().unwrap();
            while !*stop {
                stop = cvar.wait(stop).unwrap();
            }
        });

        Ok(handle)
    }

    async fn stop(&mut self) -> Result<(), String> {
        {
            let (stop, cvar) = &*self.stop;
            *stop.lock().unwrap() = true;
            cvar.notify_one();
        }

        self.unset_inputs().await;
        self.unset_outputs().await;

        Ok(())
    }
}