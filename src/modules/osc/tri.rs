use crate::data::{ DataPoint, DataSlice, DataType };
use crate::modules::{ Module };

use std::f64::consts::PI;

pub struct Tri {
    time: usize,
}

impl Tri {
    pub fn new() -> Self {
        Self {
            time: 0,
        }
    }
}

const SAMPLE_RATE: f64 = 48000.0;

const INPUT_TYPE_DEFAULTS: [(&'static str, DataType, Option<DataPoint>); 4] = [
    ("freq",   DataType::F64, Some(DataPoint::F64(440.0))),
    ("amp",    DataType::F64, Some(DataPoint::F64(1.0))),
    ("phase",  DataType::F64, Some(DataPoint::F64(0.0))),
    ("offset", DataType::F64, Some(DataPoint::F64(0.0))),
];

const OUTPUT_TYPES: [(&'static str, DataType); 1] = [
    ("out", DataType::F64),
];

impl Module for Tri {
    fn reset(&mut self) {
        self.time = 0;
    }

    fn compute(&mut self, args: &DataSlice, dst: &mut DataSlice) {
        let freq = match args[0] {
            DataPoint::F64(freq) => freq,
            _ => panic!(),
        };

        let amp = match args[1] {
            DataPoint::F64(amp) => amp,
            _ => panic!(),
        };

        let phase = match args[2] {
            DataPoint::F64(phase) => phase,
            _ => panic!(),
        };

        let offset = match args[3] {
            DataPoint::F64(offset) => offset,
            _ => panic!(),
        };

        //  https://mathworld.wolfram.com/TriangleWave.html
        let arg = (self.time as f64) * PI * freq / SAMPLE_RATE + phase;
        let val = arg.sin().asin() * amp * PI / 2.0 + offset;

        self.time += 1;

        dst[0] = DataPoint::F64(val);
    }

    fn input_type_defaults(&self) -> Box<dyn Iterator<Item=(&str, DataType, Option<DataPoint>)> + Send + Sync + '_> {
        Box::new(
            INPUT_TYPE_DEFAULTS.iter()
                .map(|(n, it, d)| (*n, *it, *d))
        )
    }

    fn output_types(&self) -> Box<dyn Iterator<Item=(&str, DataType)> + Send + Sync + '_> {
        Box::new(
            OUTPUT_TYPES.iter()
                .map(|(n, ot)| (*n, *ot))
        )
    }
}