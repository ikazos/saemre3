use crate::data::{ self, Data, DataType };
use crate::io::{ Input, Output };
use crate::modules::{ Module, Runner };

use std::collections::{ HashMap };
use std::fmt;
use std::mem;
use std::sync::{ Arc };

use async_trait::async_trait;
use tokio::sync::{ Mutex };
use tokio::task::{ JoinHandle };

pub struct Generic {
    pub inputs: HashMap<String, Input>,
    pub outputs: HashMap<String, Output>,

    module: Option<Box<dyn Module + Send + Sync>>,
    stop: Arc<Mutex<bool>>,

    buffer_size: Option<usize>,
    input_types: Vec<(String, DataType)>,
    output_types: Vec<(String, DataType)>,
}

impl fmt::Debug for Generic {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Generic")
    }
}

impl Generic {
    pub fn new(module: Box<dyn Module + Send + Sync>, buffer_size: Option<usize>) -> Self {
        let input_types: Vec<(String, DataType)> = module.input_type_defaults()
            .map(|(n, it, _)| (n.to_owned(), it))
            .collect();

        let output_types: Vec<(String, DataType)> = module.output_types()
            .map(|(n, ot)| (n.to_owned(), ot))
            .collect();

        let mut inputs = HashMap::new();
        for (name, _, default) in module.input_type_defaults() {
            assert!(inputs.insert(name.to_owned(), Input::new(default)).is_none());
        }

        let mut outputs = HashMap::new();
        for (name, _) in output_types.iter() {
            assert!(outputs.insert(name.to_owned(), Output::new()).is_none());
        }

        Self {
            inputs,
            outputs,

            module: Some(module),
            stop: Arc::new(Mutex::new(false)),

            buffer_size,
            input_types,
            output_types,
        }
    }
}

#[async_trait]
impl Runner for Generic {
    fn inputs(&self) -> Box<dyn Iterator<Item=(&str, &Input)> + Send + Sync + '_> {
        Box::new(
            self.inputs.iter()
                .map(|(n, i)| (n.as_str(), i))
        )
    }

    fn outputs(&self) -> Box<dyn Iterator<Item=(&str, &Output)> + Send + Sync + '_> {
        Box::new(
            self.outputs.iter()
                .map(|(n, o)| (n.as_str(), o))
        )
    }

    fn inputs_mut(&mut self) -> Box<dyn Iterator<Item=(&str, &mut Input)> + Send + Sync + '_> {
        Box::new(
            self.inputs.iter_mut()
                .map(|(n, i)| (n.as_str(), i))
        )
    }

    fn outputs_mut(&mut self) -> Box<dyn Iterator<Item=(&str, &mut Output)> + Send + Sync + '_> {
        Box::new(
            self.outputs.iter_mut()
                .map(|(n, o)| (n.as_str(), o))
        )
    }

    fn input_types(&self) -> Box<dyn Iterator<Item=(&str, DataType)> + Send + Sync + '_> {
        Box::new(
            self.input_types.iter()
                .map(|(n, it)| (n.as_str(), *it))
        )
    }

    fn output_types(&self) -> Box<dyn Iterator<Item=(&str, DataType)> + Send + Sync + '_> {
        Box::new(
            self.output_types.iter()
                .map(|(n, ot)| (n.as_str(), *ot))
        )
    }

    async fn start(&mut self) -> Result<JoinHandle<()>, String> {
        let mut module = mem::replace(&mut self.module, None).unwrap();
        module.reset();

        let stop = self.stop.clone();
        let buffer_size = self.buffer_size.unwrap_or(2048);

        //  Since inputs are a `HashMap`, we must get the elements in the order
        //  specified in the returned iterator in `input_types`.
        let inputs: Vec<Input> = self.input_types.iter()
            .map(|(n, _)| self.inputs.get(n).unwrap().clone())
            .collect();
        let output = self.outputs["out"].clone();

        let mut in_buffers: Vec<Data> = vec![];
        for _ in 0..inputs.len() {
            let vec: Data = data::with_default(DataType::F64, buffer_size);
            in_buffers.push(vec);
        }

        let mut out_buffer: Data = data::with_default(DataType::F64, buffer_size);

        let handle = tokio::spawn(async move {
            'bigloop: loop {
                //  Should we stop the loop?
                if *stop.lock().await {
                    break;
                }

                //  Fill the input buffers.
                for (k, input) in inputs.iter().enumerate() {

                    //  Total number of values read into the kth buffer so far.
                    let mut nvals_read: usize = 0;

                    //  While we haven't filled the kth buffer, try and keep
                    //  reading values into it.
                    while nvals_read < in_buffers[k].len() {
                        match input.read_values(&mut in_buffers[k][nvals_read..]).await {
                            Ok(nvals) => {
                                nvals_read += nvals;
                            },

                            //  If we can't read any more values, stop trying
                            //  and skip this iteration.
                            Err(_) => {
                                continue 'bigloop;
                            },
                        }
                    }
                }

                //  Compute the output values from the inputs.
                //  The results are written to the output buffer.
                let mut args: Data = data::with_default(DataType::F64, inputs.len());
                for k in 0..buffer_size {
                    for (kk, in_buffer) in in_buffers.iter().enumerate() {
                        args[kk] = in_buffer[k];
                    }

                    module.compute(&args, &mut out_buffer[k..]);
                }

                //  Write from the output buffer.
                let mut nvals_written: usize = 0;
                while nvals_written < out_buffer.len() {
                    match output.write_values(&out_buffer[nvals_written..]).await {
                        Ok(nvals) => {
                            nvals_written += nvals;
                        },

                        Err(_) => {
                            output.state_change_notify.notified().await;
                            continue 'bigloop;
                        },
                    }
                }
            }
        });
        
        Ok(handle)
    }

    async fn stop(&mut self) -> Result<(), String> {
        *self.stop.lock().await = true;

        self.unset_inputs().await;
        self.unset_outputs().await;

        Ok(())
    }
}