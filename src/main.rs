/*
https://patorjk.com/software/taag/#p=display&f=Roman&t=saemre3
                                                                     .oooo.   
                                                                   .dP""Y88b  
  .oooo.o  .oooo.    .ooooo.  ooo. .oo.  .oo.   oooo d8b  .ooooo.        ]8P' 
 d88(  "8 `P  )88b  d88' `88b `888P"Y88bP"Y88b  `888""8P d88' `88b     <88b. 
 `"Y88b.   .oP"888  888ooo888  888   888   888   888     888ooo888      `88b.
 o.  )88b d8(  888  888    .o  888   888   888   888     888    .o o.   .88P 
 8""888P' `Y888""8o `Y8bod8P' o888o o888o o888o d888b    `Y8bod8P' `8bd88P'  
*/

#![allow(dead_code)]

mod data;
mod gui;
mod help_strs;
mod io;
mod manager;
mod modules;
mod tui;

use clap::{ App, Arg };

use std::path::PathBuf;

const ABOUT: &str = r#"
================================================================================

                                                                     .oooo.   
                                                                   .dP""Y88b  
  .oooo.o  .oooo.    .ooooo.  ooo. .oo.  .oo.   oooo d8b  .ooooo.        ]8P' 
 d88(  "8 `P  )88b  d88' `88b `888P"Y88bP"Y88b  `888""8P d88' `88b     <88b. 
 `"Y88b.   .oP"888  888ooo888  888   888   888   888     888ooo888      `88b.
 o.  )88b d8(  888  888    .o  888   888   888   888     888    .o o.   .88P 
 8""888P' `Y888""8o `Y8bod8P' o888o o888o o888o d888b    `Y8bod8P' `8bd88P'  

================================================================================

An audio programming environment.
"#;

#[derive(Debug, Clone)]
pub struct Args {
    input: Option<PathBuf>,
    buffer_size: Option<usize>,
    audio_buffer_size: Option<u32>,
}

impl Default for Args {
    fn default() -> Args {
        Args {
            input: None,
            buffer_size: None,
            audio_buffer_size: None,
        }
    }
}

#[tokio::main(flavor = "multi_thread")]
async fn main() {
    let matches = App::new("saemre3")
        .about(ABOUT)
        .arg(Arg::with_name("input")
            .help("Path to input file")
            .takes_value(true))
        .arg(Arg::with_name("buffer-size")
            .long("--buffer-size")
            .help("Buffer size for generic modules")
            .takes_value(true))
        .arg(Arg::with_name("audio-buffer-size")
            .long("--audio-buffer-size")
            .help("Audio buffer size")
            .takes_value(true))
        .get_matches();

    let mut args = Args::default();

    args.input = matches.value_of("input")
        .map(|i| i.into());

    args.buffer_size = matches.value_of("buffer-size")
        .map(|bs| bs.parse().unwrap());

    args.audio_buffer_size = matches.value_of("audio-buffer-size")
        .map(|abs| abs.parse().unwrap());

    tui::repl(args).await;
}