//! Help messages that will be printed by certain commands.

/*  ----------------------------------------------------------------------------

    Command help

----------------------------------------------------------------------------  */

/// `help`
pub const ALL_COMMANDS_HELP: &str = "
List of available commands:
    add             Add a module.
    connect         Connect an output to an input.
    help            Get help for a command, or all commands.
    modhelp         Get help for a module.
    exit            Exit the program.

List of topics:
    :initializers   Module initializer list syntax.
    :selectors      Selector syntax.

Run `help <command>` to learn more about command <command>.
Run `help <topic>` to learn more topic <topic>.
";

/// `help add`
pub const ADD_COMMAND_HELP: &str = "
COMMAND
    add <name> <module-initializer-list>

DESCRIPTION
    Add a module.
    
    The module will be named <name>. The type of the module and parameter
    initializers are identified by the module initializer list
    <module-initializer-list>. To learn about module initializer list syntax,
    consult the help topic `:initializers` by running:

     >> help :initializers

ARGUMENTS
    <name>          Module name.
    <module-initializer-list>
                    Module initializer list.
";

/// `help connect`
pub const CONNECT_COMMAND_HELP: &str = "
COMMAND
    connect <output> <input>

DESCRIPTION
    Connect the output <output> to the input <input>.

    To learn about selector syntax, consult the help topic `:selectors` by
    running:

     >> help :selectors

ARGUMENTS
    <output>        Output selector.
    <input>         Input selector.
";

/// `help help`
pub const HELP_COMMAND_HELP: &str = "
COMMAND
    help
    help <command>
    help <topic>

DESCRIPTION
    Without any arguments, get a list of commands and topics for which a help
    message is available.

    When <command> is specified, get help for the command <command>.

    When <topic> is specified, get help for the topic <topic>.

ARGUMENTS
    <command>       Command to get help for.
    <topic>         Topic to get help for.
";

/// `help modhelp`
pub const MODHELP_COMMAND_HELP: &str = "
COMMAND
    modhelp
    modhelp <module>

DESCRIPTION
    Without any arguments, get a list of modules for which a help message is
    available.

    When <module> is specified, get help for the module <module>.

ARGUMENTS
    <module>        Module to get help for.
";

/// `help exit`
pub const EXIT_COMMAND_HELP: &str = "
COMMAND
    exit

DESCRIPTION
    Exit the program.
";

/*  ----------------------------------------------------------------------------

    Topic help

----------------------------------------------------------------------------  */

/// `help :initializers`
pub const INITIALIZERS_TOPIC_HELP: &str = "
Module initializer list syntax

Module initializer lists are used to initialize a model.
";

/// `help :selectors`
pub const SELECTORS_TOPIC_HELP: &str = "
Selector syntax

Selectors are used to specify a module, an input or an output. Some commands
take selectors as their arguments. For example, here is you would use the
`connect` command to connect the output from a `sine` named `sine1` to 
phase input to another `sine` named `sine2`:

    connect -sine1.out -sine2.phase

Here, `-sine1.out` and `-sine2.phase` are selectors.

Selectors are essentially a dot-separated list of identifiers. The pseudo-EBNF
below defines a selector that consists of N identifiers:

    selector = id1, \".\", id2, \".\", id3, \".\", ... \".\", idN;
";

/*  ----------------------------------------------------------------------------

    Module help

----------------------------------------------------------------------------  */

/// `modhelp`
pub const ALL_MODULES_HELP: &str = "
List of available modules:

    Math
        +, add          Add.
        -, sub          Subtract.
        *, mul          Multiply.
        /, div          Divide.
        %, fmod         Modulo.
        exp             Exponential function.
        ln              Logarithmic function.
        abs             Compute the absolute value of an argument.
        max             Compute the maximum of two arguments.
        min             Compute the minimum of two arguments.

    Oscillators
        sine            Sine wave generator.
        tri             Triangle wave generator.
        square          Square wave generator.
        sawup           Upward sawtooth wave generator.
        sawdown         Downward sawtooth wave generator.

    Miscellaneous
        delay           Delay an input signal.
        copy            Makes two copies of an input signal.
        dac             Digital-to-analog converter.

Run `modhelp <module>` to learn more about module <module>.
";

/// `modhelp abs`
pub const ABS_MODULE_HELP: &str = "
MODULE
    abs

DESCRIPTION
    Compute the absolute value of x.

INPUTS
    x           f64         Argument.

OUTPUTS
    out         f64         Output.
";

/// `modhelp add`
pub const ADD_MODULE_HELP: &str = "
MODULE
    +, add

DESCRIPTION
    Add 2 numbers together.

INPUTS
    a           f64         Argument 1.
    b           f64         Argument 2.

OUTPUTS
    out         f64         Output.
";

/// `modhelp dac`
pub const DAC_MODULE_HELP: &str = "
MODULE
    dac

DESCRIPTION
    Digital-to-analog converter.

INPUTS
    in          f64         Input.
";

/// `modhelp div`
pub const DIV_MODULE_HELP: &str = "
MODULE
    /, div

DESCRIPTION
    Divide 1 number by another.

INPUTS
    a           f64         Argument 1.
    b           f64         Argument 2.

OUTPUTS
    out         f64         Output.
";

/// `modhelp exp`
pub const EXP_MODULE_HELP: &str = "
MODULE
    exp
    
DESCRIPTION
    Calculate e raised to the power of x.

INPUTS
    x           f64         Argument.

OUTPUTS
    out         f64         Output.
";

/// `modhelp fmod`
pub const FMOD_MODULE_HELP: &str = "
MODULE
    %, fmod

DESCRIPTION
    Calculate the least nonnegative remainder of `b` divided by `a`.

INPUTS
    a           f64         Argument 1.
    b           f64         Argument 2.

OUTPUTS
    out         f64         Output.
";

/// `modhelp ln`
pub const LN_MODULE_HELP: &str = "
MODULE
    ln
    
DESCRIPTION
    Calculate the natural logarithm of x.

INPUTS
    x           f64         Argument.

OUTPUTS
    out         f64         Output.
";

/// `modhelp max`
pub const MAX_MODULE_HELP: &str = "
MODULE
    max

DESCRIPTION
    Compute the maximum of 2 numbers.

INPUTS
    a           f64         Argument 1.
    b           f64         Argument 2.

OUTPUTS
    out         f64         Output.
";

/// `modhelp min`
pub const MIN_MODULE_HELP: &str = "
MODULE
    min

DESCRIPTION
    Compute the minimum of 2 numbers.

INPUTS
    a           f64         Argument 1.
    b           f64         Argument 2.

OUTPUTS
    out         f64         Output.
";

/// `modhelp mul`
pub const MUL_MODULE_HELP: &str = "
MODULE
    *, mul

DESCRIPTION
    Multiply 2 numbers together.

INPUTS
    a           f64         Argument 1.
    b           f64         Argument 2.

OUTPUTS
    out         f64         Output.
";

/// `modhelp sawup`
pub const SAWUP_MODULE_HELP: &str = "
MODULE
    sawup

DESCRIPTION
    Upward sawtooth wave generator.

INPUTS
    freq        f64         Frequency.
    amp         f64         Amplitude.
    phase       f64         Phase.
    offset      f64         Offset.

OUTPUTS
    out         f64         Output.
";

/// `modhelp sine`
pub const SINE_MODULE_HELP: &str = "
MODULE
    sine

DESCRIPTION
    Sine wave generator.

INPUTS
    freq        f64         Frequency.
    amp         f64         Amplitude.
    phase       f64         Phase.
    offset      f64         Offset.

OUTPUTS
    out         f64         Output.
";

/// `modhelp square`
pub const SQUARE_MODULE_HELP: &str = "
MODULE
    square

DESCRIPTION
    Square wave generator.

INPUTS
    freq        f64         Frequency.
    amp         f64         Amplitude.
    phase       f64         Phase.
    offset      f64         Offset.

OUTPUTS
    out         f64         Output.
";

/// `modhelp sub`
pub const SUB_MODULE_HELP: &str = "
MODULE
    -, sub

DESCRIPTION
    Subtract 1 number from another.

INPUTS
    a           f64         Argument 1.
    b           f64         Argument 2.

OUTPUTS
    out         f64         Output.
";

/// `modhelp tri`
pub const TRI_MODULE_HELP: &str = "
MODULE
    tri

DESCRIPTION
    Triangle wave generator.

INPUTS
    freq        f64         Frequency.
    amp         f64         Amplitude.
    phase       f64         Phase.
    offset      f64         Offset.

OUTPUTS
    out         f64         Output.
";