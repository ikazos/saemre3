//! Data types supported by `saemre3`.

use std::fmt;
use std::iter;

/// Represent the type of some data.
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum DataType {
    I64, F64,
}

impl fmt::Display for DataType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &DataType::I64 => write!(f, "i64"),
            &DataType::F64 => write!(f, "f64"),
        }
    }
}

/// A single data point.
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum DataPoint {
    I64(i64), F64(f64),
}

impl DataPoint {
    /// Get the type of the data point.
    pub fn data_type(&self) -> DataType {
        match self {
            &DataPoint::I64(_) => DataType::I64,
            &DataPoint::F64(_) => DataType::F64,
        }
    }

    /// Convert a floating point number in the range [0, 1] to an `i16` in the
    /// range [0, `i16::MAX`].
    ///
    /// This is used by certain modules that handles recording to a WAV file
    /// with 16-bit integer samples.
    ///
    /// TODO: Error if conversion not possible?
    pub fn to_i16_max(self) -> i16 {
        match self {
            DataPoint::I64(x) => (x * (i16::MAX as i64)) as i16,
            DataPoint::F64(x) => (x * (i16::MAX as f64)) as i16,
        }
    }

    /// Convert a floating point number to `f32`.
    ///
    /// TODO: Error if conversion not possible?
    pub fn to_f32(self) -> f32 {
        match self {
            DataPoint::I64(x) => x as f32,
            DataPoint::F64(x) => x as f32,
        }
    }

    /// Create a data point with a specified type and a default value for that
    /// type.
    pub fn with_default(data_type: DataType) -> Self {
        match data_type {
            DataType::I64 => DataPoint::I64(i64::default()),
            DataType::F64 => DataPoint::F64(f64::default()),
        }
    }

    /// Try and parse the given string into a value of the specified type, and
    /// create a data point from the value if the parse succeeds.
    pub fn from_str_with_type(value: &str, data_type: DataType) -> Result<Self, ()> {
        let data_point = match data_type {
            DataType::I64 =>
                DataPoint::from(value.parse::<i64>().map_err(|_| ())?),

            DataType::F64 =>
                DataPoint::from(value.parse::<f64>().map_err(|_| ())?),
        };

        Ok(data_point)
    }
}

impl fmt::Display for DataPoint {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &DataPoint::I64(x) => write!(f, "{:?}", x),
            &DataPoint::F64(x) => write!(f, "{:?}", x),
        }
    }
}

impl From<i64> for DataPoint {
    fn from(x: i64) -> Self {
        DataPoint::I64(x)
    }
}

impl From<f64> for DataPoint {
    fn from(x: f64) -> Self {
        DataPoint::F64(x)
    }
}

/// `Data` is a vector of `DataPoint`s.
pub type Data = Vec<DataPoint>;

/// A data slice is a slice of data.
pub type DataSlice = [DataPoint];

/// Create data with a specified type and size, filled with default values for
/// that type.
pub fn with_default(data_type: DataType, size: usize) -> Data {
    iter::repeat(DataPoint::with_default(data_type))
        .take(size)
        .collect()
}