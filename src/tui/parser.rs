use std::fmt;

use itertools::Itertools;
use nom::branch::{ alt };
use nom::bytes::complete::{ tag, take_till1 };
use nom::character::complete::{ alpha1, alphanumeric0, space0, space1 };
use nom::combinator::{ eof, map, opt, recognize };
use nom::multi::{ many0 };
use nom::sequence::{ tuple };

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum CommandType {
    Add, Connect, Help, ModHelp, Exit,
}

fn parse_command_type_raw(s: &str) -> nom::IResult<&str, CommandType, CommandParseInternalError<&str>> {
    match alt::<_, _, CommandParseInternalError<&str>, _>((
        map(tag("add"),     |_| CommandType::Add),
        map(tag("connect"), |_| CommandType::Connect),
        map(tag("help"),    |_| CommandType::Help),
        map(tag("modhelp"), |_| CommandType::ModHelp),
        map(tag("exit"),    |_| CommandType::Exit)
    ))(s) {
        Ok(tuple) => Ok(tuple),
        Err(_) => Err(nom::Err::Error(
            CommandParseInternalError::Cpe(
                CommandParseError::UnknownCommand {
                    command: s.to_owned()
                }
            )
        )),
    }
}

fn parse_command_type(s: &str) -> Result<CommandType, CommandParseError> {
    match parse_command_type_raw(s) {
        Ok((_, command_type)) => Ok(command_type),
        Err(nom::Err::Error(CommandParseInternalError::Cpe(cpe))) => Err(cpe),
        _ => panic!(),
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum ModuleType {
    Add, Abs, Dac, Div, Exp, Fmod, Ln, Max, Min, Mul, SawUp, Sine, Square, Sub,
    Tri,
}

fn parse_module_type_raw(s: &str) -> nom::IResult<&str, ModuleType, CommandParseInternalError<&str>> {
    match alt::<_, _, CommandParseInternalError<&str>, _>((
        map(tag("+"),      |_| ModuleType::Add),
        map(tag("add"),    |_| ModuleType::Add),
        map(tag("abs"),    |_| ModuleType::Abs),
        map(tag("dac"),    |_| ModuleType::Dac),
        map(tag("/"),      |_| ModuleType::Div),
        map(tag("div"),    |_| ModuleType::Div),
        map(tag("exp"),    |_| ModuleType::Exp),
        map(tag("fmod"),   |_| ModuleType::Fmod),
        map(tag("ln"),     |_| ModuleType::Ln),
        map(tag("max"),    |_| ModuleType::Max),
        map(tag("min"),    |_| ModuleType::Min),
        map(tag("*"),      |_| ModuleType::Mul),
        map(tag("mul"),    |_| ModuleType::Mul),
        map(tag("sawup"),  |_| ModuleType::SawUp),
        map(tag("sine"),   |_| ModuleType::Sine),
        map(tag("square"), |_| ModuleType::Square),
        map(tag("-"),      |_| ModuleType::Sub),
        map(tag("sub"),    |_| ModuleType::Sub),
        map(tag("tri"),    |_| ModuleType::Tri)
    ))(s) {
        Ok(tuple) => Ok(tuple),
        Err(_) => Err(nom::Err::Error(
            CommandParseInternalError::Cpe(
                CommandParseError::UnknownModuleType {
                    module_type: s.to_owned()
                }
            )
        )),
    }
}

fn parse_module_type(s: &str) -> Result<ModuleType, CommandParseError> {
    match parse_module_type_raw(s) {
        Ok((_, module_type)) => Ok(module_type),
        Err(nom::Err::Error(CommandParseInternalError::Cpe(cpe))) => Err(cpe),
        _ => panic!(),
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Topic {
    Selectors,
}

fn parse_topic_raw(s: &str) -> nom::IResult<&str, Topic, CommandParseInternalError<&str>> {
    match alt::<_, _, CommandParseInternalError<&str>, _>((
        map(tag(":selectors"), |_| Topic::Selectors),
        map(tag(":selectors"), |_| Topic::Selectors) // Just for now
    ))(s) {
        Ok(tuple) => Ok(tuple),
        Err(_) => Err(nom::Err::Error(
            CommandParseInternalError::Cpe(
                CommandParseError::UnknownTopic {
                    topic: s.to_owned()
                }
            )
        )),
    }
}

fn parse_topic(s: &str) -> Result<Topic, CommandParseError> {
    match parse_topic_raw(s) {
        Ok((_, topic)) => Ok(topic),
        Err(nom::Err::Error(CommandParseInternalError::Cpe(cpe))) => Err(cpe),
        _ => panic!(),
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Help {
    Command(CommandType),
    Topic(Topic),
    None,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum ModHelp {
    Module(ModuleType),
    None,
}

pub type Selector = Vec<String>;

#[derive(Debug, Clone)]
pub enum Command {
    Add {
        name: String,
        module_type: ModuleType,
        initializers: Vec<(String, String)>,
    },

    Connect {
        output: Selector,
        input: Selector,
    },

    Help(Help),
    ModHelp(ModHelp),
    Exit,
}

#[derive(Debug, Clone)]
pub enum NumArgs {
    Eq(usize),
    EqList(Vec<usize>),
    Max(usize),
    Min(usize),
}

impl fmt::Display for NumArgs {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &NumArgs::Eq(x) =>
                write!(f, "exactly {}", x),

            &NumArgs::EqList(ref xs) =>
                match xs.len() {
                    0 => panic!(),
                    1 => write!(f, "exactly {}", xs[0]),
                    n => write!(
                        f,
                        "{} or {}",
                        xs[0..n-1].iter().format(", "),
                        xs[n-1]
                    ),
                },

            &NumArgs::Max(x) =>
                write!(f, "at most {}", x),

            &NumArgs::Min(x) =>
                write!(f, "at least {}", x),
        }
    }
}

#[derive(Debug, Clone)]
pub enum CommandParseError {
    UnknownCommand {
        command: String,
    },

    BadNumArgs {
        got: usize,
        needs: NumArgs,
    },

    UnknownModuleType {
        module_type: String,
    },

    UnknownTopic {
        topic: String,
    },

    //  Bad idea?
    UnknownCommandOrTopic {
        cot: String,
    },

    BadSelector {
        selector: String,
    },

    BadId {
        id: String,
    },

    TrailingGarbage {
        garbage: String,
    },

    BadInitPair {
        init_pair: String,
    },

    BadModuleInits {
        module_inits: String,
    }
}

impl fmt::Display for CommandParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &CommandParseError::UnknownCommand { ref command } =>
                write!(f, "Unknown command `{}`", command),

            &CommandParseError::BadNumArgs { ref got, ref needs } =>
                write!(f, "The specified command accepts {} arguments, got {}", needs, got),

            &CommandParseError::UnknownModuleType { ref module_type } =>
                write!(f, "Unknown module type `{}`", module_type),

            &CommandParseError::UnknownTopic { ref topic } =>
                write!(f, "Unknown topic `{}`", topic),

            &CommandParseError::UnknownCommandOrTopic { ref cot } =>
                write!(f, "Unknown command or topic `{}`", cot),

            &CommandParseError::BadSelector { ref selector } =>
                write!(f, "Bad selector `{}`", selector),

            &CommandParseError::BadId { ref id } =>
                write!(f, "Bad id `{}`", id),

            &CommandParseError::TrailingGarbage { ref garbage } =>
                write!(f, "Trailing garbage `{}`", garbage),

            &CommandParseError::BadInitPair { ref init_pair } =>
                write!(f, "Bad initializer pair `{}`", init_pair),

            &CommandParseError::BadModuleInits { ref module_inits } =>
                write!(f, "Bad module initializers `{}`", module_inits),
        }
    }
}

#[derive(Debug, Clone)]
pub enum CommandParseInternalError<I> {
    Nom(I, nom::error::ErrorKind),
    Cpe(CommandParseError),
}

impl<I> fmt::Display for CommandParseInternalError<I> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &CommandParseInternalError::Nom(_, _) =>
                write!(f, "nom error"),

            &CommandParseInternalError::Cpe(ref cpe) =>
                write!(f, "{}", cpe),
        }
    }
}

impl<I> nom::error::ParseError<I> for CommandParseInternalError<I> {
    fn from_error_kind(input: I, kind: nom::error::ErrorKind) -> Self {
        CommandParseInternalError::Nom(input, kind)
    }

    //  Is this ok? Copied from
    //  https://github.com/Geal/nom/blob/master/examples/custom_error.rs
    fn append(_: I, _: nom::error::ErrorKind, other: Self) -> Self {
        other
    }
}

#[derive(Debug, Clone)]
pub struct Arg {
    text: String,
    arg_type: ArgType,
}

#[derive(Debug, Clone)]
pub enum ArgType {
    Id(String),
    Topic(Topic),
    Selector(Vec<String>),
    ModuleInits(ModuleType, Vec<(String, String)>),
}

fn parse_id(s: &str) -> nom::IResult<&str, &str> {
    recognize(tuple((alpha1, alphanumeric0)))(s)
}

fn parse_arg_id(s: &str) -> nom::IResult<&str, Arg, CommandParseInternalError<&str>> {
    match map(
        parse_id,
        |s| Arg {
            text: s.to_owned(),
            arg_type: ArgType::Id(s.to_owned()),
        }
    )(s) {
        Ok(tuple) => Ok(tuple),
        Err(_) => Err(nom::Err::Error(
            CommandParseInternalError::Cpe(
                CommandParseError::BadId {
                    id: s.to_owned(),
                }
            )
        )),
    }
}

//  Something like:
//  :selectors
fn parse_arg_topic(s: &str) -> nom::IResult<&str, Arg, CommandParseInternalError<&str>> {
    let s = match recognize(tuple((tag(":"), parse_id)))(s) {
        Ok((_, s)) => s,
        Err(_) => {
            return Err(nom::Err::Error(
                CommandParseInternalError::Cpe(
                    CommandParseError::UnknownTopic {
                        topic: s.to_owned(),
                    }
                )
            ));
        },
    };

    match parse_topic_raw(s) {
        Ok((t, topic)) => {
            let arg = Arg {
                text: s.to_owned(),
                arg_type: ArgType::Topic(topic),
            };

            Ok((t, arg))
        },

        Err(_) => Err(nom::Err::Error(
            CommandParseInternalError::Cpe(
                CommandParseError::UnknownTopic {
                    topic: s.to_owned(),
                }
            )
        )),
    }
}

//  Something like:
//  -group1.sine1.phase
fn parse_arg_selector(s: &str) -> nom::IResult<&str, Arg, CommandParseInternalError<&str>> {
    match map(
        //  The return type of this parser is something like:
        //  (&str, &str, Vec<(&str, &str)>)
        tuple((
            tag("-"),
            parse_id,
            many0(tuple((
                tag("."),
                parse_id
            )))
        )),

        |(_, first_id, tail_ids)| {
            let mut ids: Vec<String> = vec![];
            ids.push(first_id.to_owned());
            for (_, id) in tail_ids.iter() {
                ids.push((*id).to_owned());
            }

            ArgType::Selector(ids)
        }
    )(s) {
        Ok((t, selector)) => Ok((t, Arg {
            text: s.to_owned(),
            arg_type: selector,
        })),

        Err(_) => Err(nom::Err::Error(
            CommandParseInternalError::Cpe(
                CommandParseError::BadSelector {
                    selector: format!("-{}", s.to_owned()),
                }
            )
        )),
    }
}

//  freq=440.0
//  freq = 440.0
//  freq = 89u8dv8u89vuydf9hvdf89vd09ug0df90..;';';4=-23=-
fn parse_init_pair(s: &str) -> nom::IResult<&str, (&str, &str)> {
    map(
        tuple((
            parse_id,
            recognize(tuple((
                space0, tag("="), space0
            ))),
            take_till1(|ch| ",)".contains(ch))
        )),
        |(id, _, maybe_data_point)| (id, maybe_data_point.trim_end())
    )(s)
}

//  Something like:
//  sine(freq=440.0, amp=1.0, phase=0.0, offset=1.0)
fn parse_arg_module_inits(s: &str) -> nom::IResult<&str, Arg, CommandParseInternalError<&str>> {
    //  Parse the module type...
    let (s, module_type) = parse_module_type_raw(s)?;

    //  Then the initializers...
    //  Type: (_, Option<(_, (&str, &str), Vec<(_, _, (&str, &str))>)>, _)
    match tuple((
        tag("("),
        opt(tuple((
            space0,
            parse_init_pair,
            many0(tuple((
                tag(","),
                space0,
                parse_init_pair
            )))
        ))),
        tag(")")
    ))(s) {
        Ok((t, stuff)) => {
            let (_, init_pairs, _) = stuff;
            let init_pairs: Vec<(String, String)> = match init_pairs {
                Some(init_pairs) => {
                    let (_, first_pair, tail_pairs) = init_pairs;
                    let mut args: Vec<(String, String)> = vec![
                        (first_pair.0.to_owned(), first_pair.1.to_owned())
                    ];

                    for (_, _, tail_pair) in tail_pairs {
                        args.push((tail_pair.0.to_owned(), tail_pair.1.to_owned()));
                    }
                    args
                },

                None => vec![],
            };

            let arg = Arg {
                text: s.to_owned(),
                arg_type: ArgType::ModuleInits(module_type, init_pairs),
            };
            Ok((t, arg))
        },

        Err(_) => {
            Err(nom::Err::Error(
            CommandParseInternalError::Cpe(
                CommandParseError::BadModuleInits {
                    module_inits: s.to_owned(),
                }
            )
        ))},
    }
}

fn parse_arg(s: &str) -> nom::IResult<&str, Arg, CommandParseInternalError<&str>> {
    alt((
        parse_arg_module_inits,
        parse_arg_id,
        parse_arg_topic,
        parse_arg_selector,
    ))(s)
}

fn parse_args(s: &str) -> nom::IResult<&str, Vec<Arg>, CommandParseInternalError<&str>> {
    map(
        many0(tuple((space1, parse_arg))),
        |args| {
            args.into_iter()
                .map(|(_, arg)| arg)
                .collect::<Vec<Arg>>()
        }
    )(s)
}

fn check_num_args(got: usize, needs: NumArgs) -> Result<usize, CommandParseError> {
    let cond = match &needs {
        &NumArgs::Eq(needs) =>         (got == needs),
        &NumArgs::EqList(ref needs) => (needs.contains(&got)),
        &NumArgs::Max(needs) =>        (got <= needs),
        &NumArgs::Min(needs) =>        (got >= needs),
    };

    match cond {
        true => Ok(got),
        false => Err(CommandParseError::BadNumArgs {
            got,
            needs,
        })
    }
}

fn check_add_args(mut args: Vec<Arg>) -> Result<Command, CommandParseError> {
    check_num_args(args.len(), NumArgs::Eq(2))?;

    let mut drain = args.drain(..);

    let name = match drain.next().unwrap() {
        Arg { text: _, arg_type: ArgType::Id(name) } => name,
        Arg { text, arg_type: _ } => {
            return Err(CommandParseError::BadId {
                id: text,
            });
        },
    };

    let (module_type, initializers) = match drain.next().unwrap() {
        /*
        Arg { text: _, arg_type: ArgType::Id(id) } =>
            (parse_module_type(&id)?, vec![]),
*/
        Arg { text: _, arg_type: ArgType::ModuleInits(module_type, inits) } =>
            (module_type, inits),

        Arg { text, arg_type: _ } => {
            return Err(CommandParseError::BadModuleInits {
                module_inits: text,
            });
        },
    };

    Ok(Command::Add { name, module_type, initializers, })
}

fn check_connect_args(mut args: Vec<Arg>) -> Result<Command, CommandParseError> {
    check_num_args(args.len(), NumArgs::Eq(2))?;

    let mut drain = args.drain(..);

    let selector1 = match drain.next().unwrap() {
        Arg { text: _, arg_type: ArgType::Selector(ids) } => ids,
        Arg { text, arg_type: _ } => {
            return Err(CommandParseError::BadSelector {
                selector: text,
            });
        },
    };
    
    let selector2 = match drain.next().unwrap() {
        Arg { text: _, arg_type: ArgType::Selector(ids) } => ids.clone(),
        Arg { text, arg_type: _ } => {
            return Err(CommandParseError::BadSelector {
                selector: text,
            });
        },
    };

    Ok(Command::Connect {
        output: selector1,
        input: selector2,
    })
}

fn check_help_args(mut args: Vec<Arg>) -> Result<Command, CommandParseError> {
    check_num_args(args.len(), NumArgs::Max(1))?;

    let help = match args.len() {
        0 => Help::None,
        1 => {
            let mut drain = args.drain(..);

            match drain.next().unwrap() {
                Arg { text: _, arg_type: ArgType::Id(command_type) } =>
                    Help::Command(parse_command_type(&command_type)?),

                Arg { text: _, arg_type: ArgType::Topic(topic) } =>
                    Help::Topic(topic),

                Arg { text, arg_type: _ } => {
                    return Err(CommandParseError::UnknownCommandOrTopic {
                        cot: text,
                    });
                },
            }
        },
        _ => panic!(),
    };

    Ok(Command::Help(help))
}

fn check_modhelp_args(mut args: Vec<Arg>) -> Result<Command, CommandParseError> {
    check_num_args(args.len(), NumArgs::Max(1))?;

    let modhelp = match args.len() {
        0 => ModHelp::None,
        1 => {
            let mut drain = args.drain(..);

            match drain.next().unwrap() {
                Arg { text: _, arg_type: ArgType::Id(module) } =>
                    ModHelp::Module(parse_module_type(&module)?),

                Arg { text, arg_type: _ } => {
                    return Err(CommandParseError::UnknownModuleType {
                        module_type: text,
                    });
                },
            }
        },
        _ => panic!(),
    };

    Ok(Command::ModHelp(modhelp))
}

fn parse_end_of_command(s: &str) -> nom::IResult<&str, &str> {
    recognize(tuple((
        opt(alt((
            tag("\n"),
            tag("\r\n")
        ))),
        eof
    )))(s)
}

fn parse_command(s: &str) -> Result<Command, CommandParseError> {
    //  Parse the command type...
    let (s, command_type) = match parse_command_type_raw(s) {
        Ok((s, command_type)) => (s, command_type),
        Err(nom::Err::Error(CommandParseInternalError::Cpe(cpe))) => {
            return Err(cpe);
        },
        _ => panic!(),
    };

    //  Then the args...
    let (s, args) = match parse_args(s) {
        Ok((s, args)) => (s, args),
        _ => panic!(),
    };

    //  And (optional newline )EOF!
    if let Err(_) = parse_end_of_command(s) {
        return Err(CommandParseError::TrailingGarbage {
            garbage: s.to_owned(),
        })
    }

    match command_type {
        CommandType::Add =>
            Ok(check_add_args(args)?),

        CommandType::Connect =>
            Ok(check_connect_args(args)?),

        CommandType::Help =>
            Ok(check_help_args(args)?),

        CommandType::ModHelp =>
            Ok(check_modhelp_args(args)?),

        CommandType::Exit => {
            check_num_args(args.len(), NumArgs::Eq(0))?;
            Ok(Command::Exit)
        },
    }
}

impl Command {
    pub fn parse(s: &str) -> Result<Command, CommandParseError> {
        parse_command(s)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        println!("{:?}", Command::parse("hello"));
        println!("{:?}", Command::parse("exit"));
        println!("{:?}", Command::parse("exit hello"));
        println!("{:?}", Command::parse("help"));
        println!("{:?}", Command::parse("help exit"));
        println!("{:?}", Command::parse("help :selectors"));
        println!("{:?}", Command::parse("modhelp"));
        println!("{:?}", Command::parse("modhelp sine"));
        println!("{:?}", Command::parse("modhelp :selectors"));
        println!("{:?}", Command::parse("modhelp -sine1.out"));
        println!("{:?}", Command::parse("connect -sine1.out -sine2.phase"));
        println!("{:?}", Command::parse("add sine"));
        println!("{:?}", Command::parse("add sine(freq=440.0, amp=1.0, phase=0.0, offset=0.0)"));
    }
}