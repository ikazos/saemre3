use nom::branch::{ alt };
use nom::bytes::complete::{ tag, take_till1 };
use nom::character::complete::{ alpha1, alphanumeric0, digit1, space0, space1 };
use nom::combinator::{ eof, map, opt, recognize };
use nom::multi::{ many0 };
use nom::number::complete::{ double };
use nom::sequence::{ tuple };

use std::str;

/// Identifier.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Id(pub String);

/// Value.
#[derive(Debug, Clone, PartialEq)]
pub enum Value {
    I64(i64),
    F64(f64),
}

/// Parameter list.
///
/// A vector of identifier-value pairs.
#[derive(Debug, Clone, PartialEq)]
pub struct Params(pub Vec<(Id, Value)>);

/// Selector.
///
/// A vector of identifiers.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Selector(pub Vec<Id>);

/// Commands.
#[derive(Debug, Clone, PartialEq)]
pub enum Command {
    /// Unit generator instance name, unit generator type, parameters.
    Make(Id, Id, Params),

    /// Output selector, input selector.
    Connect(Selector, Selector),

    /// Duration.
    Wait(i64),

    /// (Nothing...)
    Exit,
}

/// Command types.
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum CommandType {
    /// `make`.
    Make,
    
    /// `connect`.
    Connect,
    
    /// `wait`.
    Wait,
    
    /// `exit`.
    Exit,
}

/// Parser error.
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum ParserError<I> {
    /// Wrapper around a `nom` error.
    Nom(I, nom::error::ErrorKind),

    /// Invalid command type.
    InvalidCommandType,

    /// Input does not parse to command type `CommandType`.
    InvalidSyntaxForCommandType(CommandType),

    /// The input does not parse to some valid command, but the only reason why
    /// not is because there is trailing garbage.
    TrailingGarbage,

    /// Invalid identifier.
    InvalidId,

    /// Invalid signed 64-bit integer.
    InvalidI64,

    /// Invalid 64-bit floating point number.
    InvalidF64,

    /// Invalid value.
    InvalidValue,

    /// Invalid id-value pair.
    InvalidIdValuePair,

    /// Invalid parameter list.
    InvalidParams,

    /// Invalid selector.
    InvalidSelector,
}

/// Parser error sequence.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ParserErrorSequence<I>(Vec<ParserError<I>>);

/// Type alias to be used in parser definitions and code so that I don't go
/// crazy.
type PE<'a> = ParserError<&'a str>;
type PES<'a> = ParserErrorSequence<&'a str>;

impl<I> nom::error::ParseError<I> for ParserErrorSequence<I> {
    fn from_error_kind(input: I, kind: nom::error::ErrorKind) -> Self {
        ParserErrorSequence(vec![ParserError::Nom(input, kind)])
    }

    //  Refer to
    //  https://github.com/Geal/nom/blob/master/examples/custom_error.rs
    fn append(input: I, kind: nom::error::ErrorKind, mut other: Self) -> Self {
        other.0.push(ParserError::Nom(input, kind));
        other
    }
}

impl<I> ParserErrorSequence<I> {
    /// Similar to `nom::error::ParseError::from_error_kind`, but from a
    /// non-`nom` error.
    fn from_pe(parser_error: ParserError<I>) -> Self {
        Self(vec![parser_error])
    }

    /// Similar to `nom::error::ParseError::from_error_kind`, but from a
    /// sequence of non-`nom` errors.
    fn from_pes<T>(seq: T) -> Self
        where   T: IntoIterator<Item = ParserError<I>>
    {
        Self(seq.into_iter().collect())
    }

    /// Similar to `nom::error::ParseError::append`, but append a non-`nom`
    /// error.
    fn append_pe(mut self, other: ParserError<I>) -> Self {
        self.0.push(other);
        self
    }

    /// Similar to `nom::error::ParseError::append`, but append a non-`nom`
    /// error sequence.
    //  Learn to use `IntoIterator`!
    //  https://doc.rust-lang.org/stable/std/iter/trait.IntoIterator.html
    fn append_pes<T>(mut self, seq: T) -> Self
        where   T: IntoIterator<Item = ParserError<I>>
    {
        for pe in seq {
            self.0.push(pe);
        }
        self
    }
}

impl<I> IntoIterator for ParserErrorSequence<I> {
    type Item = ParserError<I>;
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

/// Append a parser error sequence to an existing parser error sequence wrapped
/// in `nom::Err` or create a new error from the given sequence.
fn append_or_new_pes<'a, T>(err: nom::Err<PES<'a>>, seq: T) -> nom::Err<PES<'a>>
    where   T: IntoIterator<Item = PE<'a>>
{
    match err {
        nom::Err::Error(pes) =>
            nom::Err::Error(pes.append_pes(seq)),

        _ => nom::Err::Error(PES::from_pes(seq)),
    }
}

/// Parse a command type.
///
/// # Error
///
/// On error, return the singleton error sequence consisting of
/// `ParserError::InvalidCommandType`.
fn parse_command_type(s: &str) -> nom::IResult<&str, CommandType, PES> {
    match alt::<_, _, PES, _>((
        map(tag("make"),    |_| CommandType::Make),
        map(tag("connect"), |_| CommandType::Connect),
        map(tag("wait"),    |_| CommandType::Wait),
        map(tag("exit"),    |_| CommandType::Exit)
    ))(s) {
        Ok(tuple) => Ok(tuple),
        Err(_) => Err(nom::Err::Error(
            PES::from_pe(PE::InvalidCommandType)
        )),
    }
}

/// Parse from the start-of-line up to a command type, consuming any whitespace
/// characters on its way.
///
/// # Error
///
/// On error, return the error sequence from the child parsers as-is.
fn parse_sol_to_command_type(s: &str) -> nom::IResult<&str, CommandType, PES> {
    match tuple((space0::<_, PES>, parse_command_type))(s) {
        Ok((s, (_, command_type))) => Ok((s, command_type)),
        Err(err) => Err(err),
    }
}

/// Wrapper for `parse_sol_to_command_type()` which checks if the parsed command
/// type matches an expected command type, which is provided as an argument.
///
/// If they match, return the partially consumed input string and unit `()`.
///
/// If they do not match, or if the subparser (`parse_sol_to_command_type()`)
/// fails, return error.
///
/// # Error
///
/// If the subparser fails, return the subparser error as is. Refer to
/// `parse_sol_to_command_type()` for the error it returns.
///
/// If the parsed command type and the expected command type do not match,
/// return the singleton error sequence consisting of
/// `ParserError::InvalidSyntaxForCommandType(ect)`, where `ect` is the
/// expected command type.
fn parse_sol_to_expected_command_type(s: &str, expected_command_type: CommandType) -> nom::IResult<&str, (), PES> {
    match parse_sol_to_command_type(s) {
        Ok((s, parsed_command_type)) =>
            if parsed_command_type == expected_command_type {
                Ok((s, ()))
            }
            else {
                Err(nom::Err::Error(
                    PES::from_pe(
                        PE::InvalidSyntaxForCommandType(expected_command_type)
                    )
                ))
            },

        Err(err) => Err(err),
    }
}

/// Parse end-of-line (EOF), consuming any whitespace character on its way.
///
/// # Error
///
/// On error, return the singleton error sequence consisting of
/// `ParserError::TrailingGarbage`.
fn parse_to_eol(s: &str) -> nom::IResult<&str, (), PES> {
    match tuple((space0::<_, PES>, eof))(s) {
        Ok((s, _)) => Ok((s, ())),

        Err(_) => Err(
            nom::Err::Error(
                PES::from_pe(PE::TrailingGarbage)
            )
        ),
    }
}

/// Wrapper for `parse_to_eol()` which, if the subparser fails, appends the
/// error `ParserError::InvalidSyntaxForCommandType(ct)` to the error sequence
/// where `ct` is a command type provided as an argument.
fn parse_to_eol_for_command_type(s: &str, command_type: CommandType) -> nom::IResult<&str, (), PES> {
    match parse_to_eol(s) {
        Ok((s, _)) => Ok((s, ())),

        Err(nom::Err::Error(pes)) => Err(
            nom::Err::Error(
                pes.append_pe(
                    PE::InvalidSyntaxForCommandType(command_type)
                )
            )
        ),

        //  Since `parse_to_eol()` only returns `Err(nom::Err::Error)` on error,
        //  we can guarantee that this will not panic.
        _ => panic!(),
    }
}

/// Parse an identifier.
///
/// An identifier is a sequence that consists of an alphabetical character
/// followed by zero or more alphanumerical characters. E.g. `foo` and `foo123`
/// are identifiers, but `123foo` is not.
///
/// # Error
///
/// On error, return the singleton error sequence consisting of
/// `ParserError::InvalidId`.
fn parse_id(s: &str) -> nom::IResult<&str, Id, PES> {
    match recognize::<_, _, PES, _>(tuple((alpha1, alphanumeric0)))(s) {
        Ok((s, t)) => Ok((s, Id(t.to_string()))),
        Err(_) => Err(
            nom::Err::Error(
                PES::from_pe(PE::InvalidId)
            )
        ),
    }
}

/// Parse an signed 64-bit integer.
///
/// # Error
///
/// On error, return the singleton error sequence consisting of
/// `ParserError::InvalidI64`.
fn parse_i64(s: &str) -> nom::IResult<&str, i64, PES> {
    //  disgusting type annotations...  =(
    match recognize::<_, _, PES, _>(tuple((
        opt(tag("-")),
        digit1
    )))(s) {
        Ok((s, t)) => {
            let x: i64 = t.parse().unwrap();
            Ok((s, x))
        },

        Err(_) => Err(
            nom::Err::Error(
                PES::from_pe(PE::InvalidI64)
            )
        ),
    }
}

/// Parse an 64-bit floating point number (double precision). The input must
/// contain a period `.`, `e` or `E`.
///
/// # Error
///
/// On error, return the singleton error sequence consisting of
/// `ParserError::InvalidF64`.
fn parse_f64(s: &str) -> nom::IResult<&str, f64, PES> {
    match recognize::<_, _, PES, _>(double)(s) {
        Ok((_, s)) => {
            if !(s.contains(".") || s.contains("e") || s.contains("E")) {
                return Err(nom::Err::Error(PES::from_pe(PE::InvalidF64)));
            }
        },

        Err(_) => {
            return Err(nom::Err::Error(PES::from_pe(PE::InvalidF64)));
        },
    }

    match double::<&str, PES>(s) {
        Ok((s, x)) => Ok((s, x)),
        Err(_) => Err(
            nom::Err::Error(
                PES::from_pe(PE::InvalidF64)
            )
        )
    }
}

/// Parse a value.
///
/// # Error
///
/// On error, return the singleton error sequence consisting of
/// `ParserError::InvalidValue`.
fn parse_value(s: &str) -> nom::IResult<&str, Value, PES> {
    //  We try to parse the input as a float before we try int because we want
    //  to consume as much input as possible. Consider the input "12.34".
    //  If we try to parse this as an int, we will only parse up to "12", which
    //  does correctly parse to 12, but the whole inputp should be parsed a
    //  float.
    match alt::<_, _, PES, _>((
        map(parse_f64, |x| Value::F64(x)),
        map(parse_i64, |x| Value::I64(x))
    ))(s) {
        Ok(tuple) => Ok(tuple),
        Err(_) => Err(nom::Err::Error(
            PES::from_pe(PE::InvalidValue)
        )),
    }
}

/// Parse a identifier-value pair.
fn parse_ivpair(s: &str) -> nom::IResult<&str, (Id, Value), PES> {
    match tuple((
        parse_id,
        space0,
        tag("="),
        space0,
        parse_value
    ))(s) {
        Ok((s, (id, _, _, _, value))) => Ok((s, (id, value))),
        Err(err) => {
            Err(
                append_or_new_pes(
                    err,
                    PES::from_pe(
                        PE::InvalidIdValuePair
                    )
                )
            )
        }
    }
}

/// Parse a parameter list.
fn parse_params(s: &str) -> nom::IResult<&str, Params, PES> {
    match tuple((
        tag("("),
        space0,
        opt(tuple((
            parse_ivpair,
            many0(tuple((space0, tag(","), space0, parse_ivpair)))
        ))),
        space0,
        tag(")")
    ))(s) {
        Ok((s, (_, _, opt, _, _))) => {
            match opt {
                None => Ok((s, Params(vec![]))),
                Some((first, rest)) => {
                    let mut params = vec![];
                    params.push(first);

                    for (_, _, _, ivpair) in rest {
                        params.push(ivpair);
                    }

                    Ok((s, Params(params)))
                },
            }
        },

        Err(err) => {
            Err(
                append_or_new_pes(
                    err,
                    PES::from_pe(
                        PE::InvalidParams
                    )
                )
            )
        },
    }
}

/// Parse a selector.
fn parse_selector(s: &str) -> nom::IResult<&str, Selector, PES> {
    match tuple((
        tag("-"),
        parse_id,
        many0(tuple((
            tag("."),
            parse_id
        )))
    ))(s) {
        Ok((s, (_, first, rest))) => {
            let mut ids: Vec<Id> = vec![ first ];
            for (_, id) in rest {
                ids.push(id);
            }

            Ok((s, Selector(ids)))
        },

        Err(err) => {
            Err(
                append_or_new_pes(
                    err,
                    PES::from_pe(
                        PE::InvalidSelector
                    )
                )
            )
        }
    }
}

/// Parse the `make` command.
fn parse_make_command(s: &str) -> nom::IResult<&str, Command, PES> {
    let s: &str = parse_sol_to_expected_command_type(s, CommandType::Make)?.0;

    let (s, name): (&str, Id) = match tuple((space1::<_, PES>, parse_id))(s) {
        Ok((s, (_, name))) => (s, name),

        Err(err) => {
            return Err(
                append_or_new_pes(
                    err,
                    PES::from_pe(
                        PE::InvalidSyntaxForCommandType(CommandType::Make)
                    )
                )
            );
        },
    };

    let (s, ty): (&str, Id) = match tuple((space1::<_, PES>, parse_id))(s) {
        Ok((s, (_, ty))) => (s, ty),

        Err(err) => {
            return Err(
                append_or_new_pes(
                    err,
                    PES::from_pe(
                        PE::InvalidSyntaxForCommandType(CommandType::Make)
                    )
                )
            );
        },
    };

    let (s, params): (&str, Params) = match tuple((space1::<_, PES>, parse_params))(s) {
        Ok((s, (_, params))) => (s, params),

        Err(err) => {
            return Err(
                append_or_new_pes(
                    err,
                    PES::from_pe(
                        PE::InvalidSyntaxForCommandType(CommandType::Make)
                    )
                )
            );
        },
    };

    let (s, _) = parse_to_eol_for_command_type(s, CommandType::Make)?;
    Ok((s, Command::Make(name, ty, params)))
}

/// Parse the `connect` command.
fn parse_connect_command(s: &str) -> nom::IResult<&str, Command, PES> {
    let s: &str = parse_sol_to_expected_command_type(s, CommandType::Connect)?.0;

    let (s, selector1): (&str, Selector) = match tuple((space1::<_, PES>, parse_selector))(s) {
        Ok((s, (_, selector1))) => (s, selector1),

        Err(err) => {
            return Err(
                append_or_new_pes(
                    err,
                    PES::from_pe(
                        PE::InvalidSyntaxForCommandType(CommandType::Connect)
                    )
                )
            );
        },
    };

    let (s, selector2): (&str, Selector) = match tuple((space1::<_, PES>, parse_selector))(s) {
        Ok((s, (_, selector2))) => (s, selector2),

        Err(err) => {
            return Err(
                append_or_new_pes(
                    err,
                    PES::from_pe(
                        PE::InvalidSyntaxForCommandType(CommandType::Connect)
                    )
                )
            );
        },
    };

    let (s, _) = parse_to_eol_for_command_type(s, CommandType::Connect)?;
    Ok((s, Command::Connect(selector1, selector2)))
}

/// Parse the `wait` command.
///
/// # Error
///
/// On error, return an error sequence which ends with
/// `ParserError::InvalidSyntaxForCommandType(CommandType::Wait)`.
fn parse_wait_command(s: &str) -> nom::IResult<&str, Command, PES> {
    let s: &str = parse_sol_to_expected_command_type(s, CommandType::Wait)?.0;

    let (s, x): (&str, i64) = match tuple((space1::<_, PES>, parse_i64))(s) {
        Ok((s, (_, x))) => (s, x),

        Err(err) => {
            return Err(
                append_or_new_pes(
                    err,
                    PES::from_pe(
                        PE::InvalidSyntaxForCommandType(CommandType::Make)
                    )
                )
            );
        },
    };

    let (s, _) = parse_to_eol_for_command_type(s, CommandType::Wait)?;
    Ok((s, Command::Wait(x)))
}

/// Parse the `exit` command.
///
/// # Error
///
/// On error, return an error sequence which ends with
/// `ParserError::InvalidSyntaxForCommandType(CommandType::Exit)`.
fn parse_exit_command(s: &str) -> nom::IResult<&str, Command, PES> {
    let s: &str = parse_sol_to_expected_command_type(s, CommandType::Exit)?.0;

    let (s, _) = parse_to_eol_for_command_type(s, CommandType::Exit)?;
    Ok((s, Command::Exit))
}

/// Parse into a command.
pub fn parse_command(s: &str) -> Result<Command, PES> {
    match alt((
        parse_make_command,
        parse_connect_command,
        parse_wait_command,
        parse_exit_command
    ))(s) {
        Ok((_, command)) => Ok(command),
        Err(nom::Err::Error(pes)) => Err(pes),
        Err(_) => panic!(),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_command_type() {
        assert_eq!(
            parse_command_type("make"),
            Ok(("", CommandType::Make))
        );
        
        assert_eq!(
            parse_command_type("connect"),
            Ok(("", CommandType::Connect))
        );
        
        assert_eq!(
            parse_command_type("wait"),
            Ok(("", CommandType::Wait))
        );
        
        assert_eq!(
            parse_command_type("exit"),
            Ok(("", CommandType::Exit))
        );

        assert!(
            parse_command_type("hello").is_err()
        );
    }

    #[test]
    fn test_parse_id() {
        assert_eq!(
            parse_id("foo"),
            Ok(("", Id(format!("foo"))))
        );
        
        assert_eq!(
            parse_id("foo123"),
            Ok(("", Id(format!("foo123"))))
        );

        assert!(
            parse_id("123foo").is_err()
        );
    }

    #[test]
    fn test_parse_i64() {
        assert_eq!(
            parse_i64("1234"),
            Ok(("", 1234))
        );

        assert_eq!(
            parse_i64("-1234"),
            Ok(("", -1234))
        );

        assert_eq!(
            parse_i64("12345678"),
            Ok(("", 12345678))
        );

        assert_eq!(
            parse_i64("123456789012"),
            Ok(("", 123456789012))
        );

        assert_eq!(
            parse_i64("9223372036854775807"),
            Ok(("", i64::MAX))
        );

        assert_eq!(
            parse_i64("-9223372036854775808"),
            Ok(("", i64::MIN))
        );
    }

    #[test]
    fn test_parse_f64() {
        assert_eq!(
            parse_f64("12.34"),
            Ok(("", 12.34))
        );

        assert_eq!(
            parse_f64("-12.34"),
            Ok(("", -12.34))
        );
    }

    #[test]
    fn test_parse_value() {
        assert_eq!(
            parse_value("1234"),
            Ok(("", Value::I64(1234)))
        );

        assert_eq!(
            parse_value("12.34"),
            Ok(("", Value::F64(12.34)))
        );

        assert_eq!(
            parse_value("1e-6"),
            Ok(("", Value::F64(1e-6)))
        );

        assert_eq!(
            parse_value("1E-6"),
            Ok(("", Value::F64(1E-6)))
        );
    }

    #[test]
    fn test_parse_ivpair() {
        assert_eq!(
            parse_ivpair("freq=440"),
            Ok(("", (Id(format!("freq")), Value::I64(440))))
        );

        assert_eq!(
            parse_ivpair("freq=440."),
            Ok(("", (Id(format!("freq")), Value::F64(440.))))
        );

        assert_eq!(
            parse_ivpair("freq =440"),
            Ok(("", (Id(format!("freq")), Value::I64(440))))
        );

        assert_eq!(
            parse_ivpair("freq= 440"),
            Ok(("", (Id(format!("freq")), Value::I64(440))))
        );

        assert_eq!(
            parse_ivpair("freq = 440"),
            Ok(("", (Id(format!("freq")), Value::I64(440))))
        );
    }

    #[test]
    fn test_parse_params() {
        assert_eq!(
            parse_params("()"),
            Ok(("", Params(vec![])))
        );

        assert_eq!(
            parse_params("( )"),
            Ok(("", Params(vec![])))
        );

        assert_eq!(
            parse_params("(freq=440)"),
            Ok(("", Params(vec![
                (Id(format!("freq")), Value::I64(440))
            ])))
        );

        assert_eq!(
            parse_params("( freq=440 )"),
            Ok(("", Params(vec![
                (Id(format!("freq")), Value::I64(440))
            ])))
        );

        assert_eq!(
            parse_params("(freq=440, phase=.5)"),
            Ok(("", Params(vec![
                (Id(format!("freq")), Value::I64(440)),
                (Id(format!("phase")), Value::F64(0.5))
            ])))
        );

        assert_eq!(
            parse_params("( freq=440 , phase=.5 )"),
            Ok(("", Params(vec![
                (Id(format!("freq")), Value::I64(440)),
                (Id(format!("phase")), Value::F64(0.5))
            ])))
        );
    }

    #[test]
    fn test_parse_selector() {
        assert_eq!(
            parse_selector("-mysine"),
            Ok(("", Selector(vec![
                Id(format!("mysine"))
            ])))
        );

        assert_eq!(
            parse_selector("-mysine.out"),
            Ok(("", Selector(vec![
                Id(format!("mysine")),
                Id(format!("out"))
            ])))
        );

        assert_eq!(
            parse_selector("-mygroup.mysine.out"),
            Ok(("", Selector(vec![
                Id(format!("mygroup")),
                Id(format!("mysine")),
                Id(format!("out"))
            ])))
        );
    }

    #[test]
    fn test_parse_make_command() {
        assert_eq!(
            parse_make_command("make mysine sine ()"),
            Ok(("", Command::Make(
                Id(format!("mysine")),
                Id(format!("sine")),
                Params(vec![])
            )))
        );

        assert_eq!(
            parse_make_command("make mysine sine ( freq=440. , phase=.5 )"),
            Ok(("", Command::Make(
                Id(format!("mysine")),
                Id(format!("sine")),
                Params(vec![
                    (Id(format!("freq")), Value::F64(440.)),
                    (Id(format!("phase")), Value::F64(0.5))
                ])
            )))
        );
    }

    #[test]
    fn test_parse_connect_command() {
        assert_eq!(
            parse_connect_command("connect -mysine.out -mydac.in"),
            Ok(("", Command::Connect(
                Selector(vec![
                    Id(format!("mysine")),
                    Id(format!("out"))
                ]),
                Selector(vec![
                    Id(format!("mydac")),
                    Id(format!("in"))
                ])
            )))
        );
    }

    #[test]
    fn test_parse_wait_command() {
        assert_eq!(
            parse_wait_command("wait 100"),
            Ok(("", Command::Wait(100)))
        );

        assert_eq!(
            parse_wait_command("wait 12345678"),
            Ok(("", Command::Wait(12345678)))
        );

        assert_eq!(
            parse_wait_command("wait 123456789012"),
            Ok(("", Command::Wait(123456789012)))
        );

        assert_eq!(
            parse_wait_command("   wait 100"),
            Ok(("", Command::Wait(100)))
        );

        assert_eq!(
            parse_wait_command("wait 100   "),
            Ok(("", Command::Wait(100)))
        );

        assert_eq!(
            parse_wait_command("   wait 100   "),
            Ok(("", Command::Wait(100)))
        );

        assert_eq!(
            parse_wait_command("wait   100   "),
            Ok(("", Command::Wait(100)))
        );

        assert_eq!(
            parse_wait_command("   wait   100"),
            Ok(("", Command::Wait(100)))
        );

        assert_eq!(
            parse_wait_command("   wait   100   "),
            Ok(("", Command::Wait(100)))
        );
    }

    #[test]
    fn test_parse_exit_command() {
        assert_eq!(
            parse_exit_command("exit"),
            Ok(("", Command::Exit))
        );

        assert_eq!(
            parse_exit_command("   exit"),
            Ok(("", Command::Exit))
        );
        
        assert_eq!(
            parse_exit_command("exit   "),
            Ok(("", Command::Exit))
        );

        assert_eq!(
            parse_exit_command("   exit   "),
            Ok(("", Command::Exit))
        );
    }
}