use nom::branch::{ alt };
use nom::bytes::complete::{ tag };
use nom::character::complete::{ anychar, digit1 };
use nom::combinator::{ map, opt, recognize, verify };
use nom::multi::{ many0 };
use nom::number::complete::{ double };
use nom::sequence::{ tuple };

#[derive(Debug, Clone, PartialEq, Eq)]
enum Token<'a> {
    Id(&'a str),
    HyphenId(&'a str),
    Integer(&'a str),
    Double(&'a str),
    Period,
    LeftParen,
    RightParen,
    Equals,
    Comma,
}

fn lex_char(input: &str) -> nom::IResult<&str, Token> {
    alt((
        map(tag("."), |_| Token::Period),
        map(tag("("), |_| Token::LeftParen),
        map(tag(")"), |_| Token::RightParen),
        map(tag("="), |_| Token::Equals),
        map(tag(","), |_| Token::Comma)
    ))(input)
}

fn lex_integer(input: &str) -> nom::IResult<&str, Token> {
    map(
        recognize(tuple((
            opt(tag("-")),
            digit1
        ))),
        |s| Token::Integer(s)
    )(input)
}

fn lex_double(input: &str) -> nom::IResult<&str, Token> {
    map(
        verify(
            recognize(double),
            |s: &str| {
                s.contains('.') ||
                s.contains('e') ||
                s.contains('E')
            }
        ),
        |s| Token::Double(s)
    )(input)
}

fn lex_id_raw(input: &str) -> nom::IResult<&str, &str> {
    recognize(tuple((
        //  First character is either _ or alpha.
        verify(
            anychar,
            |c: &char| c.is_alphabetic() || *c == '_'
        ),
        //  Second+ characters are either _ or alphanumeric.
        many0(verify(
            anychar,
            |c: &char| c.is_alphanumeric() || *c == '_'
        ))
    )))(input)
}

fn lex_id(input: &str) -> nom::IResult<&str, Token> {
    map(
        lex_id_raw,
        |s| Token::Id(s)
    )(input)
}

fn lex_hyphen_id(input: &str) -> nom::IResult<&str, Token> {
    map(
        recognize(tuple((
            tag("-"),
            lex_id_raw
        ))),
        |s| Token::HyphenId(s)
    )(input)
}

fn lex_token(input: &str) -> nom::IResult<&str, Token> {
    alt((
        lex_double,
        lex_integer,
        lex_char,
        lex_hyphen_id,
        lex_id
    ))(input)
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
struct LexerError {
    pub position: usize,
}

fn lex<'a>(mut input: &'a str) -> Result<Vec<Token<'a>>, LexerError> {
    let original_input = input;
    let mut tokens: Vec<Token<'a>> = vec![];

    input = input.trim_start();

    while !input.is_empty() {
        match lex_token(input) {
            Ok((new_input, token)) => {
                tokens.push(token);
                input = new_input;
            },

            Err(_) => {
                return Err(LexerError {
                    position: original_input.len() - input.len(),
                });
            },
        }

        input = input.trim_start();
    }

    Ok(tokens)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_lex_char() {
        assert_eq!(
            lex_char("."),
            Ok(("", Token::Period))
        );

        assert_eq!(
            lex_char("("),
            Ok(("", Token::LeftParen))
        );

        assert_eq!(
            lex_char(")"),
            Ok(("", Token::RightParen))
        );

        assert_eq!(
            lex_char("="),
            Ok(("", Token::Equals))
        );
    }

    #[test]
    fn test_lex_integer() {
        assert_eq!(
            lex_integer("1234"),
            Ok(("", Token::Integer("1234")))
        );

        assert_eq!(
            lex_integer("-1234"),
            Ok(("", Token::Integer("-1234")))
        );
    }

    #[test]
    fn test_lex_double() {
        assert_eq!(
            lex_double("12.34"),
            Ok(("", Token::Double("12.34")))
        );

        assert_eq!(
            lex_double("-12.34"),
            Ok(("", Token::Double("-12.34")))
        );
    }

    #[test]
    fn test_lex_id_raw() {
        assert_eq!(
            lex_id_raw("hello"),
            Ok(("", "hello"))
        );

        assert_eq!(
            lex_id_raw("hello123"),
            Ok(("", "hello123"))
        );

        assert_eq!(
            lex_id_raw("_hello"),
            Ok(("", "_hello"))
        );

        assert_eq!(
            lex_id_raw("_hello123"),
            Ok(("", "_hello123"))
        );

        assert_eq!(
            lex_id_raw("_hello_"),
            Ok(("", "_hello_"))
        );

        assert_eq!(
            lex_id_raw("_hello_123"),
            Ok(("", "_hello_123"))
        );
    }

    #[test]
    fn test_lex_id() {
        assert_eq!(
            lex_id("hello"),
            Ok(("", Token::Id("hello")))
        );

        assert_eq!(
            lex_id("hello123"),
            Ok(("", Token::Id("hello123")))
        );

        assert_eq!(
            lex_id("_hello"),
            Ok(("", Token::Id("_hello")))
        );

        assert_eq!(
            lex_id("_hello123"),
            Ok(("", Token::Id("_hello123")))
        );
    }

    #[test]
    fn test_lex_hyphen_id() {
        assert_eq!(
            lex_hyphen_id("-hello"),
            Ok(("", Token::HyphenId("-hello")))
        );

        assert_eq!(
            lex_hyphen_id("-hello123"),
            Ok(("", Token::HyphenId("-hello123")))
        );

        assert_eq!(
            lex_hyphen_id("-_hello"),
            Ok(("", Token::HyphenId("-_hello")))
        );

        assert_eq!(
            lex_hyphen_id("-_hello123"),
            Ok(("", Token::HyphenId("-_hello123")))
        );
    }

    #[test]
    fn test_lex() {
        assert_eq!(
            lex("make mysine sine ()"),
            Ok(vec![
                Token::Id("make"),
                Token::Id("mysine"),
                Token::Id("sine"),
                Token::LeftParen,
                Token::RightParen,
            ])
        );

        assert_eq!(
            lex("make mysine sine (freq=440., amp=.5)"),
            Ok(vec![
                Token::Id("make"),
                Token::Id("mysine"),
                Token::Id("sine"),
                Token::LeftParen,
                Token::Id("freq"),
                Token::Equals,
                Token::Double("440."),
                Token::Comma,
                Token::Id("amp"),
                Token::Equals,
                Token::Double(".5"),
                Token::RightParen,
            ])
        );

        assert_eq!(
            lex("connect -mygroup.mysine.out -mygroup.mydac.in"),
            Ok(vec![
                Token::Id("connect"),
                Token::HyphenId("-mygroup"),
                Token::Period,
                Token::Id("mysine"),
                Token::Period,
                Token::Id("out"),
                Token::HyphenId("-mygroup"),
                Token::Period,
                Token::Id("mydac"),
                Token::Period,
                Token::Id("in"),
            ])
        )
    }
}