mod newparser;
mod novelparser;

use crate::{ Args };
use crate::manager::{ Manager, Message, MessageSender, ModuleType };
use crate::data::{ DataPoint };

use std::convert::{ TryFrom };
use std::fmt;
use std::fs::File;
use std::io::{ BufReader, BufRead, Write };

use tokio::join;
use tokio::time::{ self, Duration };

#[derive(Debug, Clone)]
enum CommandRuntimeError {
    InvalidModuleType,
}

impl fmt::Display for CommandRuntimeError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &CommandRuntimeError::InvalidModuleType =>
                write!(f, "invalid module type"),
        }
    }
}

fn convert_module_type(module_type: &str) -> Result<ModuleType, ()> {
    match module_type {
        "dac" =>    Ok(ModuleType::Dac),
        "sawup" =>  Ok(ModuleType::SawUp),
        "sine" =>   Ok(ModuleType::Sine),
        "square" => Ok(ModuleType::Square),
        "tri" =>    Ok(ModuleType::Tri),
        _ => Err(()),
    }
}

fn convert_value_to_datapoint(value: newparser::Value) -> DataPoint {
    match value {
        newparser::Value::I64(x) => DataPoint::I64(x),
        newparser::Value::F64(x) => DataPoint::F64(x),
    }
}

/// Test!
async fn make(
    sender: &mut MessageSender,
    name: newparser::Id,
    module_type: newparser::Id,
    initializers: newparser::Params,
) -> Result<(), CommandRuntimeError> {
    let name: String = name.0;

    let module_type: ModuleType = convert_module_type(&module_type.0)
        .map_err(|_| CommandRuntimeError::InvalidModuleType)?;

    let initializers: Vec<(String, DataPoint)> = initializers.0
        .into_iter()
        .map(|(id, v)| {
            (id.0, convert_value_to_datapoint(v))
        })
        .collect();

    sender.send(Message::Make(name, module_type, initializers)).await.unwrap();

    Ok(())
}

async fn connect(
    sender: &mut MessageSender,
    mut output: newparser::Selector,
    mut input: newparser::Selector
) -> Result<(), CommandRuntimeError> {
    let mut output_drain = output.0.drain(..);
    let mut input_drain = input.0.drain(..);

    sender.send(Message::Connect(
        output_drain.next().unwrap().0,
        output_drain.next().unwrap().0,
        input_drain.next().unwrap().0,
        input_drain.next().unwrap().0
    )).await.unwrap();

    Ok(())
}

async fn parse_and_run_command(s: &str, sender: &mut MessageSender) -> Result<bool, String> {
    let command: newparser::Command = match newparser::parse_command(s) {
        Ok(command) => command,
        Err(err) => {
            return Err(format!("Error parsing input: {:?}", err));
        },
    };

    match command {
        newparser::Command::Make(name, module_type, initializers) => {
            if let Err(e) = make(sender, name, module_type, initializers).await {
                return Err(format!("Runtime error: {}", e));
            };
        },

        newparser::Command::Connect(output, input) => {
            if let Err(e) = connect(sender, output, input).await {
                return Err(format!("Runtime error: {}", e));
            };
        },

        newparser::Command::Wait(millis) => {
            match u64::try_from(millis) {
                Ok(millis) => {
                    time::sleep(Duration::from_millis(millis)).await;
                },

                Err(_) => {
                    return Err(format!("You can't wait for a negative number of milliseconds..."));
                },
            }
        },

        newparser::Command::Exit => {
            sender.send(Message::Exit).await.unwrap();
            println!("Hej då! =)");
            return Ok(false);
        },
    }

    Ok(true)
}

pub async fn repl(args: Args) {
    println!("saemre3 -- type in a command.");
    { std::io::stdout().flush().unwrap(); }
    
    let (manager, mut sender) = Manager::new(&args);
    let handle = manager.run().await;

    let mut run_loop = true;
    if let Some(input_path) = args.input {
        let file = File::open(input_path).unwrap();
        let reader = BufReader::new(file);

        for line in reader.lines() {
            let line = line.unwrap();

            match parse_and_run_command(line.trim(), &mut sender).await {
                Ok(keep_running) => {
                    if !keep_running {
                        run_loop = false;
                        break;
                    }
                },
    
                Err(err) => {
                    println!("{}", err);
                }
            }
        }
    }

    if run_loop {
        loop {
            let line = {
                let mut buffer = String::new();
                { std::io::stdout().flush().unwrap(); }
                { std::io::stderr().flush().unwrap(); }
                print!(" >> ");
                { std::io::stdout().flush().unwrap(); }
                std::io::stdin().read_line(&mut buffer).unwrap();
                buffer
            };
    
            match parse_and_run_command(line.trim(), &mut sender).await {
                Ok(keep_running) => {
                    if !keep_running {
                        break;
                    }
                },
    
                Err(err) => {
                    println!("{}", err);
                }
            }
        }
    }

    if let (Err(_),) = join!(handle) {
        eprintln!("REPL thread terminated with error");
    }
}