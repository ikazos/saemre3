//! The "manager", i.e. the struct that manages all the modules and the
//! connections between them.

use crate::{ Args };
use crate::data::{ DataPoint };
use crate::modules::{ Dac, Generic, Module, Runner, SawUp, Sine, Square, Tri };
//  use crate::modules::{ Abs, Add, Dac, Div, Exp, Fmod, Generic, Ln, Max, Min,
//    Module, Mul, Runner, SawUp, Sine, Square, Sub, Tri };

use std::collections::HashMap;
use std::io::Write;

use tokio::{ join };
use tokio::sync::mpsc;
use tokio::task::{ JoinHandle };

/// Module identifier type.
pub type ModuleId = String;

/// Parameter identifier type.
pub type ParamId = String;

/// Module type.
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum ModuleType {
//  Abs, Add, Dac, Div, Exp, Fmod, Ln, Max, Min, Mul, SawUp, Sine, Square, Sub,
//  Tri,
    Dac, SawUp, Sine, Square, Tri,
}

impl ModuleType {
    /// Return `true` iff the module is a generic module, i.e. should be run
    /// with the module runner `crate::modules::Generic`.
    fn is_generic(&self) -> bool {
        match self {
            ModuleType::Dac => false,
            _ => true,
        }
    }
}

/// A message that can be sent to and will be processed by the manager.
#[derive(Debug, Clone, PartialEq)]
pub enum Message {
    /// Add a new module.
    Make(ModuleId, ModuleType, Vec<(String, DataPoint)>),

    /// Connect a module output parameter to a module input parameter.
    Connect(ModuleId, ParamId, ModuleId, ParamId),

    /// Exit the program.
    Exit,
}

/// A struct that manages all created modules and connections.
/// 
/// The `Manager` struct is like the back end of `saemre3`. It does two things:
///
/// 1.  It stores all the modules and their thread handles, i.e. handles to the
///     threads where the modules are running.
/// 2.  It receives and processes messages, which are sent by front ends like
///     `tui`.
pub struct Manager {
    /// A map from module identifiers to the module runners they identify.
    runners: HashMap<ModuleId, Box<dyn Runner + Send + Sync>>,

    /// A vector of module runner thread handles.
    ///
    /// TODO: Make this a `HashMap` as well, because we need to be able to
    /// identify a thread handle by module ID in order to implement a command
    /// that deletes a module.
    handles: Vec<JoinHandle<()>>,

    /// The receiver side of a bounded `tokio::mpsc` channel through which
    /// `Manager` receives messages.
    msg_receiver: mpsc::Receiver<Message>,

    args: Args,
}

/// Message sender type.
pub type MessageSender = mpsc::Sender<Message>;

/// Size of the message channel.
/// TODO: Make this configurable?
const CHANNEL_BUFFER: usize = 10;

impl Manager {
    /// Create an empty manager.
    ///
    /// Returns a tuple `(manager, msg_sender)` where:
    ///
    /// *   `manager` is the `Manager` just created; and
    /// *   `msg_sender` is the sender side of the bounded `tokio::mpsc`
    ///     channel through which `manager` receives and processes messages.
    ///     One can send `Message`s through `msg_sender` to tell `manager` what
    ///     to do.
    pub fn new(args: &Args) -> (Self, MessageSender) {
        let (msg_sender, msg_receiver) = mpsc::channel(CHANNEL_BUFFER);
        let manager = Self {
            runners: HashMap::new(),
            handles: vec![],
            msg_receiver,
            args: args.clone(),
        };

        (manager, msg_sender)
    }

    /// Add a module.
    ///
    /// The new module will have name `id`. Use `params` to specify the module
    /// type to create and any default values for the module parameters.
    ///
    /// # Errors
    ///
    /// Returns `Err(error_message)` if:
    ///
    /// *   Another module with the same name already exists.
    async fn make_module(
        &mut self,
        id: ModuleId,
        module_type: ModuleType,
        params: Vec<(String, DataPoint)>
    ) -> Result<(), ()> {
        if self.runners.contains_key(&id) {
            return Err(());
        }

        let mut runner: Box<dyn Runner + Send + Sync> =
            if module_type.is_generic() {
                let module: Box<dyn Module + Send + Sync> = match module_type {
                    //  ModuleType::Abs =>    Box::new(Abs::new()),
                    //  ModuleType::Add =>    Box::new(Add::new()),
                    //  ModuleType::Div =>    Box::new(Div::new()),
                    //  ModuleType::Exp =>    Box::new(Exp::new()),
                    //  ModuleType::Fmod =>   Box::new(Fmod::new()),
                    //  ModuleType::Ln =>     Box::new(Ln::new()),
                    //  ModuleType::Max =>    Box::new(Max::new()),
                    //  ModuleType::Min =>    Box::new(Min::new()),
                    //  ModuleType::Mul =>    Box::new(Mul::new()),
                    ModuleType::SawUp =>  Box::new(SawUp::new()),
                    ModuleType::Sine =>   Box::new(Sine::new()),
                    ModuleType::Square => Box::new(Square::new()),
                    //  ModuleType::Sub =>    Box::new(Sub::new()),
                    ModuleType::Tri =>    Box::new(Tri::new()),
                    _ => panic!(),
                };

                Box::new(Generic::new(module, self.args.buffer_size))
            }
            else {
                match module_type {
                    ModuleType::Dac => Box::new(Dac::new(
                        self.args.audio_buffer_size
                    )),
                    _ => panic!(),
                }
            };

        for (name, value) in params.iter() {
            runner.set_input_to_default(name.as_str(), *value)
                .await
                .map_err(|e| {
                    eprintln!("error: {}", e);
                })?;
        }

        self.handles.push(runner.start().await.unwrap());

        if let Some(_) = self.runners.insert(id, runner) {
            panic!();
        }

        Ok(())
    }

    /// Connect a module output parameter to a module input parameter.
    ///
    /// Specifically, the output parameter is identified as the parameter named
    /// `pid1` on the module named `mid1`, and the input parameter is
    /// identified as the parameter named `pid2` on the module named `mid2`.
    ///
    /// # Errors
    ///
    /// Returns `Err(error_message)` if:
    ///
    /// *   The modules `mid1` and/or `mid2` do not exist.
    /// *   The module named `mid1` does not have an output parameter called
    ///     `pid1`.
    /// *   The module named `mid2` does not have an input parameter called
    ///     `pid2`.
    async fn connect(
        &mut self,
        mid1: ModuleId,
        pid1: ParamId,
        mid2: ModuleId,
        pid2: ParamId
    ) -> Result<(), ()> {
        let (tx, rx) = mpsc::channel(1024);
        let runner1 = self.runners.get(&mid1).ok_or(())?;
        let output = runner1.outputs()
            .find(|(name, _)| *name == pid1)
            .ok_or(())?.1;

        let runner2 = self.runners.get(&mid2).ok_or(())?;
        let input = runner2.inputs()
            .find(|(name, _)| *name == pid2)
            .ok_or(())?.1;

        input.set_receiver(rx).await;
        output.set_sender(tx).await;

        Ok(())
    }

    /// Exit the program.
    ///
    /// Signal all running modules to stop, wait for each thread to terminate.
    async fn exit(&mut self) {
        for (_, mut runner) in self.runners.drain() {
            runner.stop().await.unwrap();
        }

        for handle in self.handles.drain(..) {
            join!(handle).0.unwrap();
        }
    }

    /// Process a message.
    ///
    /// Returns `false` iff the message was `Message::Exit`.
    async fn process_msg(&mut self, msg: Message) -> bool {
        match msg {
            Message::Make(mid, mty, params) =>
                self.make_module(mid, mty, params).await.unwrap_or_else(|_| {
                    eprintln!("command failed.");
                }),

            Message::Connect(mid1, pid1, mid2, pid2) =>
                self.connect(mid1, pid1, mid2, pid2).await.unwrap_or_else(|_| {
                    eprintln!("command failed.");
                }),

            Message::Exit => {
                self.exit().await;
                return false;
            },
        };

        true
    }

    /// Spawns a thread to run the message processing loop.
    ///
    /// Returns the handle to the message processing thread.
    ///
    /// If `Message::Exit` is received, the loop is terminated after the
    /// message is processed.
    pub async fn run(mut self) -> JoinHandle<()> {
        let handle = tokio::spawn(async move {
            while let Some(msg) = self.msg_receiver.recv().await {
                std::io::stdout().flush().unwrap();

                if !self.process_msg(msg).await {
                    break;
                }
            }
        });

        handle
    }
}