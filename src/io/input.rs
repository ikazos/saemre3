//! Inputs, from which data (`DataPoint` or `Data`) is received.

use crate::data::DataPoint;

use std::mem;
use std::sync::{ Arc };

use tokio::sync::{ mpsc, Mutex, Notify };

/// The source of an input.
///
/// Could be void, a constant, or a receiver.
#[derive(Debug)]
enum InputSrc {
    /// Void.
    ///
    /// An attempt to read from a void input source will always fail.
    Void,

    /// A constant `DataPoint`.
    ///
    /// An attempt to read from a constant input source will always succeed.
    Constant(DataPoint),

    /// A `DataPoint` receiver.
    ///
    /// Specifically, the receiver side of a bounded `tokio::mpsc` channel of
    /// `DataPoint`s.
    Receiver(mpsc::Receiver<DataPoint>),
}

impl InputSrc {
    pub fn from_opt(opt: Option<DataPoint>) -> Self {
        match opt {
            Some(dp) => InputSrc::Constant(dp),
            None => InputSrc::Void,
        }
    }

    /// Asynchronously read a single value from the input source.
    ///
    /// Return `Some(x)` if a value `x` is available and `None` otherwise.
    ///
    /// The behavior of this function depends on the variant of the input
    /// source.
    ///
    /// *   If the input source is void, then the read fails immediately.
    ///     Return `None`.
    /// *   If the input source is a constant, then the read succeeds
    ///     immediately. Return `Some(c)` where `c` is the constant.
    /// *   If the input source is a receiver, then this function attempts to
    ///     read a value from the receiver by calling
    ///     `tokio::mpsc::Receiver::recv` on it.
    pub async fn read_value(&mut self) -> Option<DataPoint> {
        match self {
            InputSrc::Void =>               None,
            InputSrc::Constant(value) =>    Some(*value),
            InputSrc::Receiver(receiver) => receiver.recv().await,
        }
    }

    /// Synchronously read a single value from the input source, blocking until
    /// the read succeeds or fails.
    ///
    /// Return `Some(x)` if a value `x` is available and `None` otherwise.
    ///
    /// The behavior of this function depends on the variant of the input
    /// source.
    ///
    /// *   If the input source is void, then the read fails immediately.
    ///     Return `None`.
    /// *   If the input source is a constant, then the read succeeds
    ///     immediately. Return `Some(c)` where `c` is the constant.
    /// *   If the input source is a receiver, then this function attempts to
    ///     read a value from the receiver by calling
    ///     `tokio::mpsc::Receiver::blocking_recv` on it.
    pub fn blocking_read_value(&mut self) -> Option<DataPoint> {
        match self {
            InputSrc::Void =>               None,
            InputSrc::Constant(value) =>    Some(*value),
            InputSrc::Receiver(receiver) => receiver.blocking_recv(),
        }
    }

    /// Asynchronously read values from the input source and attempt to fill
    /// `slice`.
    ///
    /// Return `Ok(nvals)` if the read is successful where `nvals` is the
    /// number of values read and written to `slice`. Return `Err(())`
    /// otherwise.
    ///
    /// The behavior of this function depends on the variant of the input
    /// source. Assuming `slice_len` is the length of `slice`:
    ///
    /// *   If the input source is void, then the read fails immediately.
    ///     `slice` will not be modified. Return `Err(())`.
    /// *   If the input source is a constant, then the read succeeds
    ///     immediately. `slice` will be filled with the constant. Return
    ///     `Some(slice_len)`.
    /// *   If the input source is a receiver, then this function attempts to
    ///     fill `slice` with values read by calling
    ///     `tokio::mpsc::Receiver::recv` on the receiver. Return `Some(n)`
    ///     where `n` is the number of values read from the receiver (`n` <=
    ///     `slice_len`).
    pub async fn read_values(&mut self, slice: &mut [DataPoint]) -> Result<usize, ()> {
        match self {
            InputSrc::Void => Err(()),

            InputSrc::Constant(value) => {
                slice.fill(*value);
                Ok(slice.len())
            },

            InputSrc::Receiver(receiver) => {
                let mut nvals: usize = 0;
                for value_mut in slice.iter_mut() {
                    match receiver.recv().await {
                        Some(value) => {
                            *value_mut = value;
                            nvals += 1;
                        },

                        None => {
                            break;
                        },
                    }
                }

                Ok(nvals)
            },
        }
    }

    /// Synchronously read values from the input source and attempt to fill
    /// `slice`, blocking until all reads succeed or the first failure.
    ///
    /// Return `Ok(nvals)` if the read is successful where `nvals` is the
    /// number of values read and written to `slice`. Return `Err(())`
    /// otherwise.
    ///
    /// The behavior of this function depends on the variant of the input
    /// source. Assuming `slice_len` is the length of `slice`:
    ///
    /// *   If the input source is void, then the read fails immediately.
    ///     `slice` will not be modified. Return `Err(())`.
    /// *   If the input source is a constant, then the read succeeds
    ///     immediately. `slice` will be filled with the constant. Return
    ///     `Some(slice_len)`.
    /// *   If the input source is a receiver, then this function attempts to
    ///     fill `slice` with values read by calling
    ///     `tokio::mpsc::Receiver::blocking_recv` on the receiver. Return
    ///     `Some(n)` where `n` is the number of values read from the receiver
    ///     (`n` <= `slice_len`).
    pub fn blocking_read_values(&mut self, slice: &mut [DataPoint]) -> Result<usize, ()> {
        match self {
            InputSrc::Void => Err(()),

            InputSrc::Constant(value) => {
                slice.fill(*value);
                Ok(slice.len())
            },

            InputSrc::Receiver(receiver) => {
                let mut nvals: usize = 0;
                for value_mut in slice.iter_mut() {
                    match receiver.blocking_recv() {
                        Some(value) => {
                            *value_mut = value;
                            nvals += 1;
                        },

                        None => {
                            break;
                        },
                    }
                }

                Ok(nvals)
            },
        }
    }
}

/// An input.
#[derive(Debug, Clone)]
pub struct Input {
    /// The underlying input source.
    src: Arc<Mutex<InputSrc>>,

    /// The (optional) default value for this input.
    pub default: Option<DataPoint>,

    /// A `tokio::sync::Notify` object that sends a notification when the
    /// underlying input source has changed state.
    pub state_change_notify: Arc<Notify>,
}

impl Input {
    /// Create a new input with name `name` and default value `default`.
    pub fn new(default: Option<DataPoint>) -> Self {
        Self {
            src: Arc::new(Mutex::new(
                InputSrc::from_opt(default)
            )),

            default,
            state_change_notify: Arc::new(Notify::new()),
        }
    }

    pub fn set_default(&mut self, default: Option<DataPoint>) {
        self.default = default;
    }

    /// Set the underlying source of this input to receiver `receiver`.
    ///
    /// Also send a notification through `self.state_change_notify`.
    ///
    /// Return `true` if the input source was originally a receiver, which is
    /// closed. Otherwise, return `false`.
    pub async fn set_receiver(&self, receiver: mpsc::Receiver<DataPoint>) -> bool {
        let new_src = InputSrc::Receiver(receiver);
        let old_src_mut: &mut InputSrc = &mut (*&mut self.src.lock().await);

        self.state_change_notify.notify_one();

        match mem::replace(old_src_mut, new_src) {
            InputSrc::Receiver(mut old_receiver) => {
                old_receiver.close();
                true
            },

            _ => false,
        }
    }

    /// Set the underlying source of this input to a constant that always
    /// produces the default value if it is provided; otherwise, it is set to
    /// void.
    ///
    /// Also send a notification through `self.state_change_notify`.
    ///
    /// Return `true` if the input source was originally a receiver, which is
    /// closed. Otherwise, return `false`.
    pub async fn unset_receiver(&self) -> bool {
        let new_src = match self.default {
            Some(default) => InputSrc::Constant(default),
            None => InputSrc::Void,
        };

        let old_src_mut: &mut InputSrc = &mut (*&mut self.src.lock().await);

        self.state_change_notify.notify_one();

        match mem::replace(old_src_mut, new_src) {
            InputSrc::Receiver(mut old_receiver) => {
                old_receiver.close();
                true
            },

            _ => false,
        }
    }

    /// Asynchronously read a single value from the input source.
    ///
    /// Return `Some(x)` if a value `x` is available and `None` otherwise.
    pub async fn read_value(&self) -> Option<DataPoint> {
        self.src.lock().await.read_value().await
    }

    /// Synchronously read a single value from the input source, blocking until
    /// the lock on the input source is acquired and the read from the source
    /// succeeds or fails.
    ///
    /// Return `Some(x)` if a value `x` is available and `None` otherwise.
    pub fn blocking_read_value(&self) -> Option<DataPoint> {
        loop {
            if let Ok(mut src) = self.src.try_lock() {
                return src.blocking_read_value();
            }
        }
    }

    /// Asynchronously read values from the input source and attempt to fill
    /// `slice`.
    ///
    /// Return `Ok(nvals)` if the read is successful where `nvals` is the
    /// number of values read and written to `slice`. Return `Err(())`
    /// otherwise.
    pub async fn read_values(&self, slice: &mut [DataPoint]) -> Result<usize, ()> {
        self.src.lock().await.read_values(slice).await
    }

    /// Synchronously read values from the input source and attempt to fill
    /// `slice`, blocking until the lock on the input source is acquired and
    /// all reads from the source succeed or the first failure is encountered.
    ///
    /// Return `Ok(nvals)` if the read is successful where `nvals` is the
    /// number of values read and written to `slice`. Return `Err(())`
    /// otherwise.
    pub fn blocking_read_values(&self, slice: &mut [DataPoint]) -> Result<usize, ()> {
        loop {
            if let Ok(mut src) = self.src.try_lock() {
                return src.blocking_read_values(slice);
            }
        }
    }
}