//! Inputs and outputs, through which modules pass data to each other.

mod input;
mod output;

pub use input::Input;
pub use output::Output;