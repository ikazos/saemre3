use crate::data::{ DataPoint };

use std::fmt;
use std::mem;
use std::sync::{ Arc };

use tokio::sync::{ mpsc, Mutex, Notify };

enum OutputDst {
    Void,
    Sender(mpsc::Sender<DataPoint>),
}

impl fmt::Debug for OutputDst {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "OutputDst")
    }
}

impl OutputDst {
    pub async fn write_value(&mut self, value: DataPoint) -> Result<(), ()> {
        match self {
            OutputDst::Void => Err(()),
            OutputDst::Sender(sender) => {
                match sender.send(value).await {
                    Ok(_) => Ok(()),
                    Err(_) => Err(()),
                }
            }
        }
    }
    
    pub fn blocking_write_value(&mut self, value: DataPoint) -> Result<(), ()> {
        match self {
            OutputDst::Void => Err(()),
            OutputDst::Sender(sender) => {
                match sender.blocking_send(value) {
                    Ok(_) => Ok(()),
                    Err(_) => Err(()),
                }
            }
        }
    }

    pub async fn write_values(&mut self, slice: &[DataPoint]) -> Result<usize, ()> {
        match self {
            OutputDst::Void => Err(()),
            OutputDst::Sender(sender) => {
                let mut nvals: usize = 0;
                for value in slice.iter() {
                    match sender.send(*value).await {
                        Ok(_) => {
                            nvals += 1;
                        },

                        Err(_) => {
                            break;
                        },
                    }
                }

                Ok(nvals)
            },
        }
    }

    pub fn blocking_write_values(&mut self, slice: &[DataPoint]) -> Result<usize, ()> {
        match self {
            OutputDst::Void => Err(()),
            OutputDst::Sender(sender) => {
                let mut nvals: usize = 0;
                for value in slice.iter() {
                    match sender.blocking_send(*value) {
                        Ok(_) => {
                            nvals += 1;
                        },

                        Err(_) => {
                            break;
                        },
                    }
                }

                Ok(nvals)
            },
        }
    }
}

#[derive(Debug, Clone)]
pub struct Output {
    dst: Arc<Mutex<OutputDst>>,
    pub state_change_notify: Arc<Notify>,
}

impl Output {
    pub fn new() -> Self {
        Self {
            dst: Arc::new(Mutex::new(OutputDst::Void)),
            state_change_notify: Arc::new(Notify::new()),
        }
    }

    pub async fn set_sender(&self, sender: mpsc::Sender<DataPoint>) -> bool {
        let new_dst = OutputDst::Sender(sender);
        let old_dst_mut: &mut OutputDst = &mut (*&mut self.dst.lock().await);

        self.state_change_notify.notify_one();

        match mem::replace(old_dst_mut, new_dst) {
            OutputDst::Sender(_) => true,
            _ => false,
        }
    }

    pub async fn unset_sender(&self) -> bool {
        let new_dst = OutputDst::Void;
        let old_dst_mut: &mut OutputDst = &mut (*&mut self.dst.lock().await);

        self.state_change_notify.notify_one();

        match mem::replace(old_dst_mut, new_dst) {
            OutputDst::Sender(_) => true,
            _ => false,
        }
    }

    pub async fn write_value(&self, value: DataPoint) -> Result<(), ()> {
        self.dst.lock().await.write_value(value).await
    }

    pub fn blocking_write_value(&self, value: DataPoint) -> Result<(), ()> {
        loop {
            if let Ok(mut dst) = self.dst.try_lock() {
                return dst.blocking_write_value(value);
            }
        }
    }

    pub async fn write_values(&self, slice: &[DataPoint]) -> Result<usize, ()> {
        self.dst.lock().await.write_values(slice).await
    }

    pub fn blocking_write_values(&self, slice: &[DataPoint]) -> Result<usize, ()> {
        loop {
            if let Ok(mut dst) = self.dst.try_lock() {
                return dst.blocking_write_values(slice);
            }
        }
    }
}