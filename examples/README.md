# Examples

These examples are mostly for determining the sources of any issues encountered
when running the full-fledged `saemre3` in your system.

Run any example `ex` with:

```
cargo run --example ex
```

To get help for `ex`:

```
cargo run --example ex -- -h
```