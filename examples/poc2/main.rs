use clap::{ App, Arg };

use cpal::{ BufferSize, SampleRate, StreamConfig };
use cpal::traits::{ DeviceTrait, HostTrait, StreamTrait };

use tokio::sync::mpsc;
use tokio::task;

use std::f64::consts::PI;

const ABOUT: &str = "Proof-of-concept 2

Test for `cpal` + `tokio`.

Spawns two threads, A and B. A generates a 440 Hz sine wave and sends it to B.
B takes the signal and plays it through the default host, default device and
the first supported output config. The signal is passed between the threads via
a bounded `tokio::mpsc` channel.
";

fn pick_sample_rate(min_sample_rate: SampleRate, max_sample_rate: SampleRate) -> SampleRate {
    let min_sample_rate: u32 = min_sample_rate.0;
    let max_sample_rate: u32 = max_sample_rate.0;

    if (44100 >= min_sample_rate) && (44100 <= max_sample_rate) {
        SampleRate(44100)
    }
    else if (48000 >= min_sample_rate) && (48000 <= max_sample_rate) {
        SampleRate(48000)
    }
    else {
        SampleRate(max_sample_rate)
    }
}

#[tokio::main(flavor = "multi_thread")]
async fn main() {
    let matches = App::new("poc2")
        .about(ABOUT)
        .arg(Arg::with_name("channel-size")
            .long("--channel-size")
            .default_value("2048")
            .help("Bounded `tokio::mpsc` channel size")
            .takes_value(true))
        .arg(Arg::with_name("dac-buffer-size")
            .long("--dac-buffer-size")
            .help("DAC buffer size")
            .takes_value(true))
        .get_matches();

    let channel_size: usize =
        matches.value_of("channel-size").unwrap().parse().unwrap();

    let dac_buffer_size: Option<u32> =
            match matches.value_of("dac-buffer-size") {
                Some(dbs) => Some(dbs.parse().unwrap()),
                None => None,
        };

    //  Make channel.
    let (sender, mut receiver) = mpsc::channel::<f64>(channel_size);

    //  Build host, device and stream config.
    let host = cpal::default_host();
    println!("Host id name: {}", host.id().name());

    let device = host.default_output_device()
        .expect("no output device available");
    println!("Device name: {}", device.name().unwrap());

    let mut supported_configs_range = device.supported_output_configs()
        .expect("error while querying configs");

    let supported_config_range = supported_configs_range.next()
        .expect("no supported config?!");

    let min_sample_rate = supported_config_range.min_sample_rate();
    let max_sample_rate = supported_config_range.max_sample_rate();
    let sample_rate = pick_sample_rate(min_sample_rate, max_sample_rate);

    let supported_config = supported_config_range
        .with_sample_rate(sample_rate);

    let mut config: StreamConfig = supported_config.into();
    if let Some(dbs) = dac_buffer_size {
        config.buffer_size = BufferSize::Fixed(dbs);
    }

    let sample_rate = (config.sample_rate.0) as usize;
    let channel_count = config.channels as usize;

    println!("Sample rate: {}", sample_rate);
    println!("Channel count: {}", channel_count);

    //  Spawn sine thread.
    let sine_handle = tokio::spawn(async move {
        let mut clock: usize = 0;
        let freq: f64 = 440.0;
        let sample_rate: f64 = sample_rate as f64;

        loop {
            let arg: f64 = (clock as f64) * PI * 2.0 * freq / sample_rate;
            let val: f64 = arg.sin();

            sender.send(val).await.unwrap_or_else(|err| {
                eprintln!("Error sending value {}: {}", val, err);
            });
            clock += 1;
        }
    });

    //  Spawn DAC thread.
    let dac_handle = task::spawn_blocking(move || {
        let stream = device.build_output_stream(
            &config,
            move |data: &mut [f32], _cbinfo: &cpal::OutputCallbackInfo| {
                // react to stream events and read or write stream data here.
                let chunks = data.chunks_exact_mut(channel_count);
                for slice in chunks {
                    let val = receiver.blocking_recv().unwrap_or_else(|| {
                        eprintln!("Error receiving value: got None.");
                        0.0
                    });
                    slice.fill(val as f32);
                }
            },
            move |err| {
                // react to errors here.
                eprintln!("Callback error: {}", err);
            },
        )
            .unwrap_or_else(|err| {
                eprintln!("Failed to build output stream: {}", err);
                std::process::exit(1);
            });
    
        stream.play().unwrap();

        loop {
            std::thread::park();
        }
    });

    //  Wait for both threads to terminate (never!)
    tokio::join!(sine_handle, dac_handle).0
        .unwrap_or_else(|err| {
            eprintln!("Error joining handles: {}", err);
            std::process::exit(1);
        });
}