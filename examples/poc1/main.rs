use clap::{ App, Arg };

use cpal::{ BufferSize, SampleRate, StreamConfig };
use cpal::traits::{ DeviceTrait, HostTrait, StreamTrait };

use std::f64::consts::PI;

const ABOUT: &str = "Proof-of-concept 1

Plays a 440 Hz sine wave through the default host, default device and the first
supported output config. Basically a `cpal` test.
";

/// Entry point.
///
/// Parses command-line arguments and kicks off our program!

#[tokio::main(flavor = "multi_thread")]
async fn main() {
    let matches = App::new("poc1")
        .about(ABOUT)
        .arg(Arg::with_name("dac-buffer-size")
            .long("--dac-buffer-size")
            .help("DAC buffer size")
            .takes_value(true))
        .get_matches();

    let buffer_size: Option<u32> =
        match matches.value_of("dac-buffer-size") {
            Some(dbs) => Some(dbs.parse().unwrap()),
            None => None,
        };

    sine(buffer_size).await;
}

/// Pick an appropriate sample rate within the given range.
///
/// This (somewhat) takes care of WASAPI which seems to only allow a sample
/// rate of 48 kHz.
fn pick_sample_rate(min_sample_rate: SampleRate, max_sample_rate: SampleRate) -> SampleRate {
    let min_sample_rate: u32 = min_sample_rate.0;
    let max_sample_rate: u32 = max_sample_rate.0;

    //  First, if we can pick 44.1 kHz, then pick that.
    if (44100 >= min_sample_rate) && (44100 <= max_sample_rate) {
        SampleRate(44100)
    }
    //  Otherwise, if we can pick 48 kHz, then do that.
    else if (48000 >= min_sample_rate) && (48000 <= max_sample_rate) {
        SampleRate(48000)
    }
    //  If all else fails, just pick the minimum sample rate the system
    //  supports.
    else {
        SampleRate(min_sample_rate)
    }
}

/// The main part of this program.
///
/// Set up `cpal`, build a stream whose audio data callback computes a 440 Hz
/// sine wave and writes the waveform into the buffer. Start the stream and
/// enter an infinite loop.
async fn sine(buffer_size: Option<u32>) {
    let host = cpal::default_host();
    println!("Host id name: {}", host.id().name());

    let device = host.default_output_device()
        .expect("no output device available");
    println!("Device name: {}", device.name().unwrap());

    let mut supported_configs_range = device.supported_output_configs()
        .expect("error while querying configs");

    let supported_config_range = supported_configs_range.next()
        .expect("no supported config?!");

    let min_sample_rate = supported_config_range.min_sample_rate();
    let max_sample_rate = supported_config_range.max_sample_rate();
    let sample_rate = pick_sample_rate(min_sample_rate, max_sample_rate);

    let supported_config = supported_config_range
        .with_sample_rate(sample_rate);

    let mut config: StreamConfig = supported_config.into();
    if let Some(dbs) = buffer_size {
        config.buffer_size = BufferSize::Fixed(dbs);
    }

    let sample_rate = (config.sample_rate.0) as usize;
    let channel_count = config.channels as usize;

    println!("Sample rate: {}", sample_rate);
    println!("Channel count: {}", channel_count);

    let mut clock: usize = 0;
    let freq: f64 = 440.0;
    let sample_rate: f64 = sample_rate as f64;

    let stream = device.build_output_stream(
        &config,
        move |data: &mut [f32], _cbinfo: &cpal::OutputCallbackInfo| {
            // react to stream events and read or write stream data here.
            let channel_len: usize = data.len() / channel_count;

            let chunks = data.chunks_exact_mut(channel_count);
            for (k, slice) in chunks.enumerate() {
                let arg: f64 = ((clock + k) as f64) * PI * 2.0 * freq / sample_rate;
                let val: f64 = arg.sin();
                slice.fill(val as f32);
            }

            clock += channel_len;
        },
        move |err| {
            // react to errors here.
            eprintln!("Callback error: {}", err);
        },
    )
        .unwrap_or_else(|err| {
            eprintln!("Failed to build output stream: {}", err);
            std::process::exit(1);
        });

    stream.play().unwrap();

    tokio::signal::ctrl_c().await.unwrap();
}